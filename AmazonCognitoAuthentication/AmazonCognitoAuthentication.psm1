﻿#requires -Version 5


function Open-CognitoSession {
    <#
    .Synopsis
        Establishes a login session with the Amazon Cognito authentication service.

    .Description
        Use `Open-CognitoSession` to establish the initial connection Amazon Cognito session, or refresh an existing session.

        `Open-CognitoSession` will return session tokens for the new or refreshed session.

        Use `Export-CognitoSessionTokens` to save session tokens for refresh later.

        Use `Import-CognitoSessionTokens` to load session tokens, and pass to `Open-CognitoSession` to refresh the imported session.

    .Notes
        This module requires the `Amazon.Extensions.CognitoAuthentication` NuGet package (`Install-Package Amazon.Extensions.CognitoAuthentication`).

        Disclaimer: This project is not affiliated with or endorsed by Amazon.

    .Link
        https://aws.amazon.com/cognito/
    .Link
        https://github.com/aws/aws-sdk-net-extensions-cognito
    #>
    [CmdletBinding(DefaultParameterSetName = 'New')]
    [OutputType([SessionTokens])]
    param(
        # The Client Id of the service provider using Amazon Cognito.
        [Parameter(Mandatory, ParameterSetName = 'New', Position = 0, ValueFromPipelineByPropertyName)]
        [string]$ClientId
        ,
        # The User Pool Id of the service provider using Amazon Cognito.
        [Parameter(Mandatory, ParameterSetName = 'New', Position = 1, ValueFromPipelineByPropertyName)]
        [string]$PoolId
        ,
        # The Region of the service provider using Amazon Cognito.
        [Parameter(Mandatory, ParameterSetName = 'New', Position = 2, ValueFromPipelineByPropertyName)]
        [Amazon.RegionEndpoint]$Region
        ,
        # The user login credentials used to authenticate with the service provider.
        [Parameter(Mandatory, ParameterSetName = 'New', Position = 3, ValueFromPipelineByPropertyName)]
        [PSCredential]$UserCredential
        ,
        # The bundled Cognito information used to refresh or authenticate with the service provider.
        [Parameter(Mandatory, ParameterSetName = 'Refresh', Position = 0, ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [SessionTokens]$SessionTokens
        ,
        # A flag to enable refreshing an existing session.
        # If not set, the user credentials from `$SessionTokens` will be used to establish a new session.
        [Parameter(ParameterSetName = 'Refresh')]
        [switch]$Refresh
        ,
        # A flag to force logging in to a new session, without checking expiration time.
        [switch]$Force
    )

    process {
        $tokens = $SessionTokens ?? [SessionTokens]@{
            ClientId = $ClientId
            PoolId   = $PoolId
            Region   = $Region
            UserName = $UserCredential.UserName
            Password = $UserCredential.Password
        }

        # https://docs.aws.amazon.com/cognito/latest/developerguide/getting-credentials.html
        # https://docs.aws.amazon.com/sdk-for-net/v3/developer-guide/cognito-authentication-extension.html
        $anonymous = New-Object Amazon.Runtime.AnonymousAWSCredentials
        $provider = New-Object Amazon.CognitoIdentityProvider.AmazonCognitoIdentityProviderClient ($anonymous, $tokens.Region)
        $userPool = New-Object Amazon.Extensions.CognitoAuthentication.CognitoUserPool ($tokens.PoolId, $tokens.ClientId, $provider)
        $user = New-Object Amazon.Extensions.CognitoAuthentication.CognitoUser ($tokens.UserName, $tokens.ClientId, $userPool, $provider)
        $updated = $false

        # Pull any existing access tokens, and refresh if necessary.
        $cognitoSessionTokens = $tokens.TryGetCognitoSessionTokens()
        if( $cognitoSessionTokens -and $Refresh -and !$Force ) {
            $user.SessionTokens = $cognitoSessionTokens

            if( Test-RefreshRequired $user.SessionTokens ) {
                Write-Verbose "Attempting to refresh Cognito session (expire: $($user.SessionTokens.ExpirationTime.ToLocalTime()))"
                $refreshRequest = New-Object Amazon.Extensions.CognitoAuthentication.InitiateRefreshTokenAuthRequest -Property @{ AuthFlowType = [Amazon.CognitoIdentityProvider.AuthFlowType]::REFRESH_TOKEN_AUTH }
                $refreshResponse = $user.StartWithRefreshTokenAuthAsync( $refreshRequest )
                Test-TaskResult 'Refresh' $refreshResponse | ForEach-Object { $tokens.UpdateFromAuthenticationResult( $_.AuthenticationResult ) }
                $updated = $true
            }
        }

        # If login is out of date, connect directly.
        if( $Force -or (Test-RefreshRequired $user.SessionTokens) ) {
            Write-Verbose 'Logging in to Cognito session'
            $authRequest = New-Object Amazon.Extensions.CognitoAuthentication.InitiateSrpAuthRequest -Property @{ Password = (ConvertFrom-SecureString -AsPlainText $tokens.Password) }
            $authResponse = $user.StartWithSrpAuthAsync( $authRequest )
            Test-TaskResult 'Login' $authResponse -FailOnError | ForEach-Object { $tokens.UpdateFromAuthenticationResult( $_.AuthenticationResult ) }
            $updated = $true
        }

        if( -not $updated ) { Write-Verbose "Existing Cognito session tokens still valid (expire: $($tokens.TokenExpiration.ToLocalTime()))" }

        $tokens
    }
}

function ConvertFrom-CognitoSessionTokens {
    <#
    .Synopsis
        Converts Cognito session tokens to a serializable format.

    .Description
        Use `ConvertFrom-CognitoSessionTokens` to save the session tokens returned by `Open-CognitoSession` for refresh later.

    .Notes
        This module requires the `Amazon.Extensions.CognitoAuthentication` NuGet package (`Install-Package Amazon.Extensions.CognitoAuthentication`).

    .Link
        Open-CognitoSession
    .Link
        ConvertTo-CognitoSessionTokens
    #>
    [OutputType([string])]
    param(
        # The Cognito session tokens to convert.
        [Parameter(Mandatory, Position = 0, ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [SessionTokens]$SessionTokens
    )

    process {
        $serializable = Select-Object -InputObject $SessionTokens `
            -Exclude Password, Region `
            -Property *,
        @{ n = 'Region'; e = { $_.Region.SystemName } },
        @{ n = 'Password'; e = { ConvertFrom-SecureString $_.Password } }

        ConvertTo-Json -InputObject $serializable
    }
}

function ConvertTo-CognitoSessionTokens {
    <#
    .Synopsis
        Converts a serialized value to a Cognito session tokens object.

    .Description
        Use `ConvertTo-CognitoSessionTokens` to load serialized session tokens, and pass to `Open-CognitoSession` to refresh.

    .Notes
        This module requires the `Amazon.Extensions.CognitoAuthentication` NuGet package (`Install-Package Amazon.Extensions.CognitoAuthentication`).

    .Link
        Open-CognitoSession
    .Link
        ConvertFrom-CognitoSessionTokens
    #>
    [OutputType([SessionTokens])]
    param(
        # The serialized value to convert to a `SessionTokens` object.
        [Parameter(Mandatory, Position = 0, ValueFromPipeline)]
        [string]$InputObject
    )

    process {
        $tokenJson = ConvertFrom-Json -InputObject $InputObject -AsHashtable

        if( $tokenJson.Region ) {
            $tokenJson.Region = [Amazon.RegionEndpoint]::EnumerableAllRegions | Where-Object SystemName -EQ $tokenJson.Region
        }

        if( $tokenJson.Password ) {
            $tokenJson.Password = ConvertTo-SecureString $tokenJson.Password
        }

        New-Object SessionTokens -Property $tokenJson
    }
}

function Export-CognitoSessionTokens {
    <#
    .Synopsis
        Exports Cognito session tokens to a file.

    .Description
        Use `Export-CognitoSessionTokens` to save the session tokens returned by `Open-CognitoSession` for refresh later.

    .Notes
        This module requires the `Amazon.Extensions.CognitoAuthentication` NuGet package (`Install-Package Amazon.Extensions.CognitoAuthentication`).

    .Link
        Open-CognitoSession
    .Link
        Import-CognitoSessionTokens
    #>
    [OutputType([string])]
    param(
        # The path to the file to save the `SessionTokens` object.
        [Parameter(Mandatory, Position = 0, ValueFromPipelineByPropertyName)]
        [string]$Path
        ,
        # The Cognito session tokens to save.
        [Parameter(Mandatory, Position = 1, ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [SessionTokens]$SessionTokens
    )

    process {
        ConvertFrom-CognitoSessionTokens $SessionTokens | Set-Content -Path $Path
    }
}

function Import-CognitoSessionTokens {
    <#
    .Synopsis
        Imports Cognito session tokens from a file.

    .Description
        Use `Import-CognitoSessionTokens` to load serialized session tokens, and pass to `Open-CognitoSession` to refresh.

    .Notes
        This module requires the `Amazon.Extensions.CognitoAuthentication` NuGet package (`Install-Package Amazon.Extensions.CognitoAuthentication`).

    .Link
        Open-CognitoSession
    .Link
        Export-CognitoSessionTokens
    #>
    [OutputType([SessionTokens])]
    param(
        # The path to the file containing the saved `SessionTokens` object.
        [Parameter(Mandatory, Position = 0, ValueFromPipelineByPropertyName)]
        [string]$Path
    )

    process {
        Get-Content -Path $Path -Raw | ConvertTo-CognitoSessionTokens
    }
}


function Test-TaskResult( [string]$Name, $Task, [switch]$FailOnError ) {
    Write-Verbose "Waiting on $Name task..."
    try { $Task.Wait() } catch { }
    Write-Verbose "$Name task completed with $($Task.Status)"
    if( $Task.IsCompletedSuccessfully ) {
        $Task.Result
    }
    else {
        $innerException = $Task.Exception.GetBaseException()
        if( $FailOnError ) {
            throw $innerException
        }
        elseif( $VerbosePreference -ne [System.Management.Automation.ActionPreference]::SilentlyContinue ) {
            $innerException | Write-Warning
        }
    }
}

function Test-RefreshRequired( $SessionTokens ) {
    (Get-Date -AsUTC) -ge $SessionTokens.ExpirationTime
}
