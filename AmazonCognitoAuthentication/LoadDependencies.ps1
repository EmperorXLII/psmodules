﻿#requires -Version 5


Write-Verbose 'Loading AWS dependencies...'
Import-NuGet -PackageName AWSSDK.Core
Import-NuGet -PackageName AWSSDK.CognitoIdentity
Import-NuGet -PackageName AWSSDK.CognitoIdentityProvider
Import-NuGet -PackageName Amazon.Extensions.CognitoAuthentication
