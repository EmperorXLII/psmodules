﻿#requires -Version 5


class SessionTokens {
    [string]$ClientId
    [string]$PoolId
    [Amazon.RegionEndpoint]$Region
    [string]$UserName
    [securestring]$Password
    [string]$IdToken
    [string]$AccessToken
    [string]$RefreshToken
    [DateTime]$TokenExpiration

    [Amazon.Extensions.CognitoAuthentication.CognitoUserSession] TryGetCognitoSessionTokens() {
        [bool]$hasTokens = $this.IdToken -and $this.AccessToken -and $this.RefreshToken
        if( !$hasTokens ) { return $null }

        [DateTime]$expiration = $this.TokenExpiration
        return New-Object Amazon.Extensions.CognitoAuthentication.CognitoUserSession (
            $this.IdToken,
            $this.AccessToken,
            $this.RefreshToken,
            $expiration.AddHours( -1 ),
            $expiration
        )
    }

    [void] UpdateFromAuthenticationResult( [Amazon.CognitoIdentityProvider.Model.AuthenticationResultType]$authResult ) {
        $this.IdToken = $authResult.IdToken
        $this.AccessToken = $authResult.AccessToken
        $this.RefreshToken = $authResult.RefreshToken
        $this.TokenExpiration = (Get-Date -AsUTC -Second 0 -Millisecond 0).AddSeconds( $authResult.ExpiresIn )
    }

    [bool] HasExpired( ) { return $this.HasExpired( [TimeSpan]::FromMinutes( 10 ) ) }
    [bool] HasExpired( [TimeSpan]$buffer ) {
        $now = (Get-Date -AsUTC) - $buffer
        return $now -gt $this.TokenExpiration
    }
}
