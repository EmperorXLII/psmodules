﻿#requires -Version 5


function Open-EmporiaSession {
    <#
    .Synopsis
        Establishes a login session with the Amazon Cognito authentication service.

    .Description
        Use `Open-EmporiaSession` to establish the initial connection with Emporia.

    .Notes
        Uses AmazonCognitoAuthentication\Open-CognitoSession

        Disclaimer: This project is not affiliated with or endorsed by Emporia Energy.

    .Link
        Module inspired by https://github.com/magico13/PyEmVue/
    #>
    [CmdletBinding(DefaultParameterSetName = 'New')]
    [OutputType([SessionTokens])]
    param(
        # The user login credentials used to authenticate with the Emporia service provider.
        [Parameter(Mandatory, ParameterSetName = 'New', Position = 0, ValueFromPipelineByPropertyName)]
        [PSCredential]$UserCredential
        ,
        # The bundled Cognito information used to refresh or authenticate with the Emporia service provider.
        [Parameter(Mandatory, ParameterSetName = 'Refresh', Position = 0, ValueFromPipeline)]
        [SessionTokens]$SessionTokens
        ,
        # A flag to enable refreshing an existing session.
        # If not set, the user credentials from `$SessionTokens` will be used to establish a new session.
        [Parameter(ParameterSetName = 'Refresh')]
        [switch]$Refresh
        ,
        # A flag to force logging in to a new session, without checking expiration time.
        [switch]$Force
    )

    process {
        if( $SessionTokens ) {
            AmazonCognitoAuthentication\Open-CognitoSession -SessionTokens $SessionTokens -Refresh:$Refresh -Force:$Force
        }
        else {
            $UserCredential ??= Get-Credential -UserName $SessionTokens.UserName -Password $SessionTokens.Password

            $region = [Amazon.RegionEndpoint]::USEast2
            AmazonCognitoAuthentication\Open-CognitoSession `
                -ClientId '4qte47jbstod8apnfic0bunmrq' `
                -PoolId 'us-east-2_ghlOXVLi1' `
                -Region $region `
                -UserCredential $UserCredential `
                -Force:$Force
        }
    }
}

function Get-VueCustomer {
    <#
    .Synopsis
        Gets information about the Vue customer associated with the session.

    .Link
        Module inspired by https://github.com/magico13/PyEmVue/
    #>
    [OutputType([VueCustomer])]
    param(
        # The session information provided by `Open-EmporiaSession`.
        [Parameter(Mandatory, Position = 0, ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [SessionTokens]$SessionTokens
    )

    process {
        $userName = $SessionTokens.UserCredential.UserName
        $address = [VueApi]::Customer( $userName )
        Invoke-VueRequest ([ref]$SessionTokens) 'Customer' $address |
            ForEach-Object { [VueCustomer]::FromRawObject( $SessionTokens, $_ ) }
    }
}

function Get-VueDevice {
    <#
    .Synopsis
        Gets information about the Vue devices associated with the session.

    .Link
        Module inspired by https://github.com/magico13/PyEmVue/
    #>
    [OutputType([VueDevice])]
    param(
        # The session information provided by `Open-EmporiaSession`.
        [Parameter(Mandatory, Position = 0, ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [SessionTokens]$SessionTokens
    )

    process {
        $address = [VueApi]::Devices( )
        $raw = Invoke-VueRequest ([ref]$SessionTokens) 'Devices' $address |
            Select-Object -ExpandProperty 'devices' |
            ForEach-Object { @($_) + $_.devices }
        [VueDevice]::FromRawObjects( $SessionTokens, $raw )
    }
}

function Get-VueDeviceListUsage {
    <#
    .Synopsis
        Gets usage data for the channels in a Vue device.

    .Link
        Module inspired by https://github.com/magico13/PyEmVue/
    #>
    [OutputType([VueChannelUsage])]
    param(
        # The Emporia device returned by `Get-VueDevice`.
        [Parameter(Mandatory, Position = 0, ValueFromPipeline)]
        [ValidateNotNullOrEmpty()]
        [VueDevice[]]$Device
        ,
        # The usage data start time.
        [Parameter(Position = 1)]
        [DateTime]$Instant = (Get-Date).Date
        ,
        # The usage data scale duration.
        [Parameter(Position = 2)]
        [Scale]$Scale = [Scale]::Day
        ,
        # The usage data unit.
        [Parameter(Position = 3)]
        [Unit]$Unit = [Unit]::KWH
    )

    process {
        $sessionTokens = $Device[0].SessionTokens
        $deviceGids = $Device | ForEach-Object DeviceGid
        $address = [VueApi]::DeviceListUsage( $deviceGids, $Instant, $Scale, $Unit )
        $raw = Invoke-VueRequest ([ref]$sessionTokens) 'Device usage' $address |
            Select-Object -ExpandProperty 'deviceListUsages'
        [VueChannelUsage]::FromRawObjects( $sessionTokens, $raw, $Device )
    }
}

function Get-VueChartUsage {
    <#
    .Synopsis
        Gets chart data for the channels in a Vue device.

    .Link
        Module inspired by https://github.com/magico13/PyEmVue/
    #>
    [CmdletBinding(DefaultParameterSetName = 'FromStartToNow')]
    [OutputType([VueChartUsage])]
    param(
        # An Emporia channel in the `Channels` collection of a device returned by `Get-VueDevice`.
        [Parameter(Mandatory, Position = 0, ValueFromPipeline)]
        [ValidateNotNullOrEmpty()]
        [VueChannel]$Channel
        ,
        # The chart data start time.
        [Parameter(Mandatory, ParameterSetName = 'FullRange', Position = 1)]
        [Parameter(Mandatory, ParameterSetName = 'FromStartToNow', Position = 1)]
        [DateTime]$Start
        ,
        # The chart data end time.
        [Parameter(Mandatory, ParameterSetName = 'FullRange', Position = 2)]
        [Parameter(Mandatory, ParameterSetName = 'ToEnd', Position = 1)]
        [DateTime]$End
        ,
        # The earliest start time allowed, when calculating a range from a single `$End` time.
        [Parameter(ParameterSetName = 'ToEnd', Position = 2)]
        [DateTime]$MinimumStart = ([DateTime]::MinValue)
        ,
        # The latest end time allowed, when calculating a range from a single `$Start` time.
        [Parameter(ParameterSetName = 'FromStartToNow', Position = 2)]
        [DateTime]$MaximumEnd = (Get-Date -AsUTC)
        ,
        # The chart data scale duration.
        [Parameter(Position = 3)]
        [Scale]$Scale = [Scale]::Second
        # ,
        # # The chart data unit.
        # [Parameter(Position = 4)]
        # [Unit]$Unit = [Unit]::KWH
        ,
        # A flag to disable conversion from "per-hour" KWH values to "instantaneous" Watts usage.
        [switch]$SkipNormalization
        ,
        # A flag to allow multiple API calls to retrieve full results for the requested time range.
        # When set, more than one `VueChartUsage` value may be returned.
        [switch]$AllowCallAggregation
    )

    begin {
        $validationPairs = @(@{ S = 'Start'; E = 'End' }, @{ S = 'Start'; E = 'MaximumEnd' }, @{ S = 'MinimumStart'; E = 'End' })
        foreach( $pair in $validationPairs ) {
            $startName = $pair.S
            $endName = $pair.E
            if( $PSBoundParameters.ContainsKey( $startName ) -and $PSBoundParameters.ContainsKey( $endName ) ) {
                $startValue = $PSBoundParameters[$startName]
                $endValue = $PSBoundParameters[$endName]
                if( $startValue -gt $endValue ) { throw "$endName '$endValue' must be greater than $startName '$startValue'." }
            }
        }
    }

    process {
        $hourConversionFactor = [VueApi]::GetScaleConversionFactor( $Scale ) / 1000
        $delta = [VueApi]::GetScaleDelta( $Scale )

        $limit = $Scale -eq [Scale]::Second ? $script:VueLatestSampleCountLimit : $script:VueHistorySampleCountLimit
        if( !$PSBoundParameters.ContainsKey( 'Start' ) ) {
            $Start = &$delta $End -$limit
            if( $Start -lt $MinimumStart ) { $Start = $MinimumStart }
        }

        if( !$PSBoundParameters.ContainsKey( 'End' ) ) {
            $roundedCalls = $AllowCallAggregation ? [math]::Floor( ($MaximumEnd - $Start).TotalHours * $hourConversionFactor / $limit ) : 1
            $calculatedSamples = $limit * [math]::Max( 1, $roundedCalls ) - 1
            $End = &$delta $Start $calculatedSamples
            if( $End -gt $MaximumEnd ) { $End = $MaximumEnd }
        }

        $sampleCount = ($End - $Start).TotalHours * $hourConversionFactor
        $range = "(range from '$($Start.ToLocalTime())' to '$($End.ToLocalTime())' at $Scale interval produces $([int]$sampleCount) samples)"

        [int]$calls = $AllowCallAggregation ? [math]::Ceiling( $sampleCount / $limit ) : 1
        if( $calls -gt 1 ) { Write-Verbose "ⓔ Splitting chart usage into $calls API calls $range" }
        elseif( [int]$sampleCount -gt $limit ) { Write-Warning "ⓔ Chart usage limited to $limit samples $range" }

        $Unit = [Unit]::KWH
        $sessionTokens = $Channel.SessionTokens
        $lastCall = $calls - 1

        $callStart = $Start
        [int]$samplesPerCall = $sampleCount / $calls - 1
        for( $i = 0; $i -lt $calls; $i += 1 ) {
            $callEnd = $i -eq $lastCall ? $End : (&$delta $callStart $samplesPerCall)
            $address = [VueApi]::ChartUsage( $Channel.DeviceGid, $Channel.Number, $callStart, $callEnd, $Scale, $Unit )
            $raw = Invoke-VueRequest ([ref]$sessionTokens) "$($Channel.Name) Chart Usage" $address
            if( $raw -eq $null ) { break }

            [VueChartUsage]::FromRawObject( $sessionTokens, $raw, $Channel, $Scale, $SkipNormalization )

            $callStart = &$delta $callEnd
        }
    }
}


$script:VueLatestSampleCountLimit = 4000
$script:VueHistorySampleCountLimit = 800

function Test-InvokeResponse(
    [parameter(Mandatory)][string]$InvokeName,
    [parameter(Mandatory)][AllowNull()]$Response,
    [switch]$FailOnError
) {
    $description = ${Response}?.StatusDescription ?? '<no response>'
    $statusCode = ${Response}?.StatusCode ?? [Net.HttpStatusCode]::ServiceUnavailable
    Write-Verbose "ⓔ Invoked Vue $InvokeName API: $description"
    New-Event -SourceIdentifier 'EmporiaEnergy.ApiCall' -Sender $MyInvocation.MyCommand.Module -EventArguments @($InvokeName, $statusCode) | Out-Null
    if( $statusCode -eq 200 ) {
        $Response.Content
    }
    else {
        $innerException = "$statusCode ${description}:" + [Environment]::NewLine + $Response
        if( $FailOnError ) {
            throw $innerException
        }
        elseif( $VerbosePreference -ne [System.Management.Automation.ActionPreference]::SilentlyContinue ) {
            Write-Warning "ⓔ - Exception: $innerException"
        }
    }
}

function RefreshSessionTokens(
    [parameter(Mandatory)][ref][SessionTokens]$SessionTokens,
    [switch]$force
) {
    Write-Verbose "ⓔ Refreshing session token that expired at $($SessionTokens.Value.TokenExpiration)"
    $SessionTokens.Value = Open-EmporiaSession $SessionTokens.Value -Refresh -Force:$force
}

function Invoke-VueRequest(
    [parameter(Mandatory)][ref][SessionTokens]$SessionTokens,
    [parameter(Mandatory)][string]$Name,
    [parameter(Mandatory)][string]$Address,
    [scriptblock]$CleanResult = $null
) {
    if( $SessionTokens.Value.HasExpired() ) { RefreshSessionTokens $SessionTokens }

    $response = @('try', 'retry') | ForEach-Object {
        try {
            Write-Verbose "ⓔ Invoking Emporia Vue API at $Address"
            $headers = @{ 'authtoken' = $SessionTokens.Value.IdToken }
            return Invoke-WebRequest -Uri $Address -Headers $headers
        }
        catch [Microsoft.PowerShell.Commands.HttpResponseException] {
            $exception = $_.Exception.Response
            Write-Verbose "ⓔ Received failing response: $exception"

            # BadRequest (400)
            [bool]$overLimit = $exception.StatusCode -eq [Net.HttpStatusCode]::BadRequest -and $_.ErrorDetails.Message -match 'is larger than the allowed limit of (PT[\d\w]+)'
            if( $overLimit ) {
                Write-Warning "ⓔ Over '$($matches[1])' limit: $_"
                # [Xml.XmlConvert]::ToTimeSpan( $matches[1] )
                return (Add-Member -InputObject $exception -MemberType NoteProperty -Name FullResponse -Value $_ -PassThru)
            }

            # Unauthorized (401)
            [bool]$expired = $exception.StatusCode -eq [Net.HttpStatusCode]::Unauthorized
            if( !$expired ) { throw $_ }

            RefreshSessionTokens $SessionTokens -Force
        }
    } | Select-Object -First 1

    $result = Test-InvokeResponse $Name $response
    if( $CleanResult ) { $result = &$CleanResult $result }
    if( $result ) { ConvertFrom-Json $result }
}
