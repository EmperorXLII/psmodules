﻿#requires -Version 5


enum Scale {
    Second
    Minute
    QuarterHour
    Hour
    Day
    Week
    Month
    Year
}

enum Unit {
    KWH
    #USD
    #AmpHours
    #Trees
    #Gas
    #Driven
    #Carbon
}

class VueObject {
    [SessionTokens]$SessionTokens # the (refreshed) tokens for the Emporia account session
    [PSCustomObject]$RawSource    # the JSON data for the source collection this object was pulled from (same as RawObject for singular results)
    [PSCustomObject]$RawObject    # the JSON data for this object

    VueObject() {
        # https://learn-powershell.net/2013/08/03/quick-hits-set-the-default-property-display-in-powershell-on-custom-objects/
        $typeName = $this.GetType().FullName
        $typeData = Get-TypeData -TypeName $typeName
        if( !$typeData ) {
            $ignoredPropertyNames = $this.GetIgnoredPropertyNames()
            $defaultPropertyNames = Get-Member -InputObject $this -MemberType Properties |
                ForEach-Object Name |
                Where-Object { $ignoredPropertyNames -notcontains $_ }
            Update-TypeData -TypeName $typeName -DefaultDisplayPropertySet $defaultPropertyNames
        }
    }

    [string] ToString() {
        $name = $this.GetProperty( '*Name*' )
        $id = $this.GetProperty( '*Gid*' )
        if( $name -and $id ) {
            return "$name ($id)"
        }
        elseif( $id ) {
            return $this.GetType().Name + " $id"
        }
        else {
            return $name ?? ([object]$this).ToString()
        }
    }

    hidden [string[]] GetIgnoredPropertyNames() { return @('SessionTokens', 'RawSource', 'RawObject') }

    hidden [string] GetProperty( [string]$pattern ) {
        $properties = Get-Member -InputObject $this -MemberType Properties -Name $pattern | ForEach-Object Name
        return $properties | ForEach-Object { $this.$_ } | Select-Object -First 1
    }
}

class VueCustomer : VueObject {
    [string]$CustomerGid

    static [VueCustomer] FromRawObject( [SessionTokens]$sessionTokens, [PSCustomObject]$raw ) {
        return [VueCustomer]@{
            SessionTokens = $sessionTokens
            RawSource     = $raw
            RawObject     = $raw
            CustomerGid   = $raw.CustomerGid
        }
    }
}

class VueDevice : VueObject {
    [string]$DeviceGid
    [string]$DeviceName
    [VueChannel[]]$Channels

    static [VueDevice[]] FromRawObjects( [SessionTokens]$sessionTokens, [PSCustomObject[]]$raw ) {
        return $raw | Group-Object deviceGid | ForEach-Object {
            $device = [VueDevice]@{
                SessionTokens = $sessionTokens
                RawSource     = $raw
                RawObject     = $_.Group
                DeviceGid     = $_.Name
                DeviceName    = $_.Group.locationProperties.deviceName | Where-Object { $_ } | Select-Object -First 1
            }

            $device.Channels = $_.Group.channels | ForEach-Object {
                [VueChannel]::FromRawObject( $sessionTokens, $_, $device )
            }

            $device
        }
    }
}

class VueChannel : VueObject {
    [VueDevice]$Device
    [string]$DeviceGid
    [string]$Name
    [string]$Number
    [double]$Multiplier

    static [VueChannel] FromRawObject( [SessionTokens]$sessionTokens, [PSCustomObject]$raw, [VueDevice]$device ) {
        return [VueChannel]@{
            SessionTokens = $sessionTokens
            RawSource     = $raw
            RawObject     = $raw
            Device        = $device
            DeviceGid     = $device.DeviceGid
            Name          = $raw.name ?? $device.DeviceName
            Number        = $raw.channelNum
            Multiplier    = $raw.channelMultiplier ?? 1
        }
    }

    hidden [string[]] GetIgnoredPropertyNames() { return ([VueObject]$this).GetIgnoredPropertyNames() + @('DeviceGid') }
}

class VueChannelUsage : VueObject {
    [string]$DeviceGid
    [string]$ChannelNumber
    [VueChannel]$Channel
    [double]$Usage
    [double]$Percentage
    [DateTime]$Timestamp
    [Scale]$Scale
    [Unit]$Unit

    static [VueChannelUsage[]] FromRawObjects(
        [SessionTokens]$sessionTokens,
        [PSCustomObject[]]$raw,
        [VueDevice[]]$devices
    ) {
        $deviceLookup = @{}
        foreach( $device in $devices ) {
            $channelLookup = ($deviceLookup[$device.DeviceGid] ??= @{ '(device)' = $device })
            foreach( $channel in $device.Channels ) {
                $channelLookup[$channel.Number] = $channel
            }
        }

        $timestampValue = $raw.instant.ToLocalTime()
        $scaleValue = [VueApi]::GetScaleFromValue( $raw.scale )
        $unitValue = [VueApi]::GetUnitFromValue( $raw.energyUnit )
        return $raw.devices.channelUsages | ForEach-Object {
            [string]$deviceGid = $_.deviceGid
            [string]$channelNum = $_.channelNum
            [VueChannel]$channel = $deviceLookup[$deviceGid][$channelNum] `
                ?? [VueChannel]::FromRawObject( $sessionTokens, $_, $deviceLookup[$deviceGid]['(device)'] )
            [VueChannelUsage]@{
                SessionTokens = $sessionTokens
                RawSource     = $raw
                RawObject     = $_
                DeviceGid     = $deviceGid
                ChannelNumber = $channelNum
                Channel       = $channel
                Usage         = $_.usage
                Percentage    = $_.percentage
                Timestamp     = $timestampValue
                Scale         = $scaleValue
                Unit          = $unitValue
            }
        }
    }

    [string] ToString() {
        [string]$usageFormat = '0.00'
        do {
            [string]$used = $this.Usage.ToString( $usageFormat )
            $usageFormat += '0'
        } while( $this.Usage -and $used.Trim( '0' ).Length -eq 1 )

        [string]$u = $this.Unit
        [string]$s = $this.Scale
        [string]$t = $this.Timestamp
        [string]$c = $this.Channel ?? ('{0} ({1})' -f $this.ChannelNumber, $this.DeviceGid)
        return "$used $u used over $s from $t by $c"
    }

    hidden [string[]] GetIgnoredPropertyNames() { return ([VueObject]$this).GetIgnoredPropertyNames() + @('DeviceGid', 'ChannelNumber') }
}

class VueChartUsage : VueObject {
    [VueChannel]$Channel
    [double[]]$UsageList
    [DateTime]$Timestamp
    [scriptblock]$Delta
    [Scale]$Scale
    [string]$Unit # (NOTE: other [Unit]s would require other conversions for "per-hour" to "instantenous usage")

    static [VueChartUsage[]] FromRawObject(
        [SessionTokens]$sessionTokens,
        [PSCustomObject]$raw,
        [VueChannel]$channel,
        [Scale]$scale,
        #[Unit]$unit
        [bool]$skipNormalization
    ) {
        $kwhToWattConversionFactor = $skipNormalization ? 1.0 : [VueApi]::GetScaleConversionFactor( $Scale )
        return [VueChartUsage]@{
            SessionTokens = $sessionTokens
            RawSource     = $raw
            RawObject     = $raw
            Channel       = $channel
            UsageList     = $raw.usageList | ForEach-Object { if( $null -eq $_ ) { [double]::NaN } else { $_ * $kwhToWattConversionFactor } }
            Timestamp     = $raw.firstUsageInstant.ToLocalTime()
            Delta         = [VueApi]::GetScaleDelta( $Scale )
            Scale         = $scale
            Unit          = $skipNormalization ? 'KWH' : 'Watts'
        }
    }

    [string] ToString() {
        [string]$l = $this.UsageList.Count
        [string]$u = $this.Unit
        [string]$s = $this.Scale
        [string]$t = $this.Timestamp
        [string]$c = $this.Channel
        return "$l $u samples per $s from $t for $c"
    }

    hidden [string[]] GetIgnoredPropertyNames() { return ([VueObject]$this).GetIgnoredPropertyNames() + @('Delta') }
}


class VueApi {
    static hidden [string] $Root = 'https://api.emporiaenergy.com'

    static hidden [hashtable] $ScaleValues = @{
        [Scale]::Second      = '1S'
        [Scale]::Minute      = '1MIN'
        [Scale]::QuarterHour = '15MIN'
        [Scale]::Hour        = '1H'
        [Scale]::Day         = '1D'
        [Scale]::Week        = '1W'
        [Scale]::Month       = '1MON'
        [Scale]::Year        = '1Y'
    }

    static hidden [hashtable] $ScaleFactors = @{ # (correct 'KWH' sample to Watts value)
        [Scale]::Second      = 1000 * 60 * 60
        [Scale]::Minute      = 1000 * 60
        [Scale]::QuarterHour = 1000 * 4
        [Scale]::Hour        = 1000
        [Scale]::Day         = 1000 / (24)
        [Scale]::Week        = 1000 / (24 * 7)
        [Scale]::Month       = 1000 / (24 * 30)
        [Scale]::Year        = 1000 / (24 * 365.25)
    }

    static hidden [hashtable] $ScaleDeltas = @{
        [Scale]::Second      = { param( [DateTime]$timestamp, [int]$multiple = 1 ) $timestamp.AddSeconds(  1 * $multiple ) }
        [Scale]::Minute      = { param( [DateTime]$timestamp, [int]$multiple = 1 ) $timestamp.AddMinutes(  1 * $multiple ) }
        [Scale]::QuarterHour = { param( [DateTime]$timestamp, [int]$multiple = 1 ) $timestamp.AddMinutes( 15 * $multiple ) }
        [Scale]::Hour        = { param( [DateTime]$timestamp, [int]$multiple = 1 ) $timestamp.AddHours(    1 * $multiple ) }
        [Scale]::Day         = { param( [DateTime]$timestamp, [int]$multiple = 1 ) $timestamp.AddDays(     1 * $multiple ) }
        [Scale]::Week        = { param( [DateTime]$timestamp, [int]$multiple = 1 ) $timestamp.AddDays(     7 * $multiple ) }
        [Scale]::Month       = { param( [DateTime]$timestamp, [int]$multiple = 1 ) $timestamp.AddMonths(   1 * $multiple ) }
        [Scale]::Year        = { param( [DateTime]$timestamp, [int]$multiple = 1 ) $timestamp.AddYears(    1 * $multiple ) }
    }

    static hidden [hashtable] $UnitValues = @{
        [Unit]::KWH = 'KilowattHours'
        #[Unit]::USD      = 'Dollars'
        #[Unit]::AmpHours = 'AmpHours'
        #[Unit]::Trees    = 'Trees'
        #[Unit]::Gas      = 'GallonsOfGas'
        #[Unit]::Driven   = 'MilesDriven'
        #[Unit]::Carbon   = 'Carbon'
    }

    static hidden [string] GetTimeValue( [DateTime]$time ) { return $time.ToUniversalTime().ToString( 'o' ) }

    static hidden [object] GetKeyFromValue( [hashtable]$table, [string]$value ) {
        return $table.GetEnumerator() |
            Where-Object Value -EQ $value |
            Select-Object -First 1 -ExpandProperty Key
    }

    static [Scale] GetScaleFromValue( [string]$value ) { return [VueApi]::GetKeyFromValue( [VueApi]::ScaleValues, $value ) }
    static [Unit] GetUnitFromValue( [string]$value ) { return [VueApi]::GetKeyFromValue( [VueApi]::UnitValues, $value ) }


    static [double] GetScaleConversionFactor( [Scale]$scale ) { return [VueApi]::ScaleFactors[$scale] }
    static [scriptblock] GetScaleDelta( [Scale]$scale ) { return [VueApi]::ScaleDeltas[$scale] }

    static [string] Customer( [string]$email ) {
        return [VueApi]::Root + "/customers?email=$email"
    }

    static [string] Devices( ) {
        return [VueApi]::Root + '/customers/devices'
    }

    static [string] DeviceListUsage( [string[]]$deviceGids, [DateTime]$instant, [Scale]$scale, [Unit]$unit ) {
        $deviceGidsValue = $deviceGids -join '+'
        $timestampValue = [VueApi]::GetTimeValue( $instant )
        $scaleValue = [VueApi]::ScaleValues[$scale]
        $unitValue = [VueApi]::UnitValues[$unit]
        return [VueApi]::Root + "/AppAPI?apiMethod=getDeviceListUsages&deviceGids=$deviceGidsValue&instant=$timestampValue&scale=$scaleValue&energyUnit=$unitValue"
    }

    static [string] ChartUsage( [string]$deviceGid, [string]$channelNumber, [DateTime]$start, [DateTime]$end, [Scale]$scale, [Unit]$unit ) {
        $startValue = [VueApi]::GetTimeValue( $start )
        $endValue = [VueApi]::GetTimeValue( $end )
        $scaleValue = [VueApi]::ScaleValues[$scale]
        $unitValue = [VueApi]::UnitValues[$unit]
        return [VueApi]::Root + "/AppAPI?apiMethod=getChartUsage&deviceGid=$deviceGid&channel=$channelNumber&start=$startValue&end=$endValue&scale=$scaleValue&energyUnit=$unitValue"
    }
}