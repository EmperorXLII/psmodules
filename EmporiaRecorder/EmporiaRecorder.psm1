﻿#requires -Version 5


function Watch-VueChannels {
    <#
    .Synopsis
        Continuously monitors energy usage on Emporia Vue channels, recording usage data in InfluxDB.
    #>
    [CmdletBinding(DefaultParameterSetName = 'Channels')]
    param(
        # Influx connection information.
        [Parameter(Mandatory, Position = 0, ValueFromPipelineByPropertyName)]
        [InfluxConnection]$Connection
        ,
        # Emporia channels in the `Channels` collection of a device returned by `Get-VueDevice`.
        [Parameter(Mandatory, Position = 1, ValueFromPipelineByPropertyName, ParameterSetName = 'Channels')]
        [VueChannel[]]$Channels
        ,
        # Emporia customer used to retrieve device channels to monitor.
        [Parameter(Mandatory, Position = 1, ValueFromPipelineByPropertyName, ParameterSetName = 'Customer')]
        [VueCustomer]$Customer
        ,
        # The maximum number of data requests to process per iteration.
        # (In `Continuous` acquisition mode, the default value is proportional to the number of channels being processed.)
        [Parameter(Position = 2)]
        [ValidateRange( 0, [int]::MaxValue )]
        [int]$ProcessingLimit
        ,
        # The maximum amount of time to process data requests in one iteration.
        # (In `Continuous` acquisition mode, the default value is 90 minutes.)
        [Parameter(Position = 3)]
        [TimeSpan]$ProcessingTimeLimit
        ,
        # An action to run at the end of every iteration.
        [Parameter(Position = 4)]
        [scriptblock]$IterationAction
        ,
        # Skip retrieval of latest data.
        [switch]$SkipLatest
        ,
        # Skip retrieval of earliest data.
        [switch]$SkipEarliest
        ,
        # Skip calculation of gaps between recorded data.
        [switch]$SkipGaps
        ,
        # Skip retrieval of requests data.
        [switch]$SkipMissing
        ,
        # Continuously retrieve data on a loop.
        [switch]$Continuous
    )

    begin {
        if( !$PSBoundParameters.ContainsKey( 'InformationAction' ) -and $InformationPreference -eq [Management.Automation.ActionPreference]::SilentlyContinue ) {
            $InformationPreference = [Management.Automation.ActionPreference]::Continue
        }

        $cancellationSource = [Threading.CancellationTokenSource]::new()
        Write-Information "🛈 Press 'Esc' at any time to cancel, after the current step completes."

        if( !(Test-Influx $Connection) ) { Start-InfluxDaemon }

        if( $Customer ) {
            Write-Verbose "Retrieving channels from all devices for $Customer"
            $devices = $Customer | Get-VueDevice
            $Channels = $devices.Channels
            RecordApiCalls $Connection
        }

        if( !$PSBoundParameters.ContainsKey( 'ProcessingLimit' ) ) {
            $ProcessingLimit = $Continuous ? [math]::Ceiling( $Channels.Count * 1.5 ) : [int]::MaxValue
        }

        if( !$PSBoundParameters.ContainsKey( 'ProcessingTimeLimit' ) ) {
            $ProcessingTimeLimit = $Continuous ? [TimeSpan]::FromMinutes( 90 ) : [TimeSpan]::MaxValue
        }

        PurgeOldRequestedDataEvents
        if( !$global:EmporiaRecorderSession ) { $global:EmporiaRecorderSession = [PSCustomObject]@{ Iteration = 0 } }
    }

    process {
        $previouslyProcessed = -1
        do {
            $global:EmporiaRecorderSession.Iteration += 1
            if( $Continuous ) { Write-Verbose "Starting iteration $($global:EmporiaRecorderSession.Iteration)" }

            if( TestCancellation $cancellationSource ) { break }

            if( $SkipLatest -or (TestCancellation $cancellationSource) ) {
                Write-Verbose "Skipping retrieval of lastest samples from the $($Channels.Count) channels"
            }
            else {
                Write-Information "Requesting latest data for each of the $($Channels.Count) channels."
                RequestLatest $Connection $Channels $cancellationSource
            }


            if( $SkipEarliest -or (TestCancellation $cancellationSource) ) {
                Write-Verbose "Skipping retrieval of earliest samples from the $($Channels.Count) channels"
            }
            else {
                Write-Information "Retrieving first sample recorded in Influx for each of the $($Channels.Count) channels."
                RequestEarliest $Connection $Channels $cancellationSource
            }


            if( $SkipGaps -or (TestCancellation $cancellationSource) ) {
                Write-Verbose 'Skipping check for gaps in data recorded in Influx.'
            }
            else {
                Write-Information 'Checking for gaps in data recorded in Influx.'
                RequestGaps $Connection $Channels $cancellationSource
            }


            if( $SkipMissing -or (TestCancellation $cancellationSource) ) {
                Write-Verbose 'Skipping retrieval of requested data'
                if( $Continuous ) {
                    Write-Information 'Ignoring Continous request while skipping retrieval of missing data.'
                    $Continuous = $false
                }
            }
            else {
                Write-Information 'Retrieving requested data.'

                [int]$processedCount = 0
                [int]$requestCount = GetRequestedDataEventCount
                Write-Verbose "Processing $requestCount queued data requests"

                [DateTime]$processingStart = Get-Date -AsUTC
                while( $requested = GetNextRequestedDataEvent -before (Get-Date) ) {
                    if( TestCancellation $cancellationSource ) { break }

                    [TimeSpan]$elapsed = (Get-Date -AsUTC) - $processingStart
                    [TimeSpan]$remaining = $ProcessingTimeLimit - $elapsed
                    if( $remaining -lt [TimeSpan]::Zero ) {
                        $elapsedMinutes = [math]::Round( $elapsed.TotalMinutes )
                        $allowedMinutes = [math]::Round( $ProcessingTimeLimit.TotalMinutes )
                        Write-Verbose "↩ Leaving data retrieval after $elapsedMinutes minutes (limit $allowedMinutes minutes)"
                        break
                    }

                    RecordApiCalls $connection
                    $remainingCalls, $remainingMinutes = GetApiCallCountAndMinutesRemaining $connection $remaining.TotalMinutes
                    $waitSeconds = [math]::Sign( $processedCount ) * 60 * $remainingMinutes / (1 + $remainingCalls)
                    $waitTime = (Get-Date -AsUTC).AddSeconds( $waitSeconds )
                    WaitUntil $waitTime $cancellationSource

                    Write-Information "• Processing data request ($(1 + $processedCount) of $requestCount): $($requested.Description)"
                    $requested | Remove-Event
                    try {
                        ProcessRequest $Connection $requested $cancellationSource
                        $processedCount += 1
                    }
                    catch [Microsoft.PowerShell.Commands.HttpResponseException] {
                        [bool]$serverError = $_.Exception.Response.StatusCode -eq [Net.HttpStatusCode]::InternalServerError
                        if( !$serverError ) { throw $_ }

                        $errorMessage = "for server error to clear ($($_.Exception.Message))"
                        Write-Warning "Waiting one hour $errorMessage"
                        WaitUntil (Get-Date -AsUTC).AddHours( 1 ) $cancellationSource $errorMessage
                    }
                    catch [System.Net.Http.HttpRequestException] {
                        $errorMessage = "for server error to clear ($($_.Exception.Message))"
                        Write-Warning "Waiting ten minutes $errorMessage"
                        WaitUntil (Get-Date -AsUTC).AddMinutes( 10 ) $cancellationSource $errorMessage
                    }
                    catch [InfluxDB.Client.Core.Exceptions.InternalServerErrorException] {
                        $errorMessage = "for Influx error to clear ($($_.Exception.Message))"
                        Write-Warning "Waiting 5 minutes $errorMessage"
                        WaitUntil (Get-Date -AsUTC).AddMinutes( 5 ) $cancellationSource $errorMessage
                    }

                    if( $processedCount -ge $ProcessingLimit ) {
                        Write-Verbose "↩ Leaving data retrieval after processing $processedCount data requests (limit $ProcessingLimit)"
                        break
                    }
                }

                if( !$cancellationSource.IsCancellationRequested -and $processedCount -eq 0 ) {
                    Write-Verbose 'No queued data requests available before current time'
                    if( $Continuous -and $previouslyProcessed -eq 0 ) {
                        Write-Information 'Leaving Continous acquisition loop after multiple iterations with no requested data.'
                        $Continuous = $false
                    }
                }

                $previouslyProcessed = $processedCount
            }

            RecordApiCalls $Connection

            if( $IterationAction ) {
                Write-Information "Performing specified action at end of iteration $($global:EmporiaRecorderSession.Iteration)"
                & $IterationAction $global:EmporiaRecorderSession.Iteration
            }

            if( $Continuous ) {
                $next = (GetNextRequestedDataEvent -before (Get-Date)) ?? (GetNextRequestedDataEvent)
                if( $next ) {
                    $nextTime = $next.End
                    WaitUntil $nextTime.DateTime $cancellationSource "for next requested data event at '$nextTime'"
                }
            }
        } while( $Continuous )
    }

    end {
        RecordApiCalls $Connection
        $cancellationSource.Dispose()
    }
}


[CmdletBinding]
function RequestLatest( [InfluxConnection]$connection, [VueChannel[]]$channels, [Threading.CancellationTokenSource]$cancellationSource ) {
    [Timestamp]$fallbackTime = (Get-Date -Minute 0 -Second 0 -Millisecond 0 -AsUTC).AddHours( -3 )
    foreach( $channel in $channels ) {
        if( TestCancellation $cancellationSource ) { break }

        [Timestamp]$latestSampleTime = GetEndSampleTime $Connection $channel -last
        if( $latestSampleTime ) { Write-Verbose "⌚ $channel last recorded at $latestSampleTime" }
        else { Write-Verbose "⌚ $channel has no recorded samples" }

        $timestamp = $latestSampleTime ?? $fallbackTime.AddSeconds( -1 )
        $start = $timestamp.AddSeconds( 1 )
        if( $start -lt $fallbackTime ) { $start = $fallbackTime }

        $end = $start.AddSeconds( 4000 )
        [bool]$added = AddRequestedDataEvent @{
            Channel              = $channel
            Start                = $start
            AllowCallAggregation = ($end -lt (Get-Date))
        } "retrieving $channel samples since '$start'" $start $end -passthru

        if( $added ) { SetLatestRecordedExtent $connection $channel $latestSampleTime }
    }
}

[CmdletBinding]
function RequestEarliest( [InfluxConnection]$connection, [VueChannel[]]$channels, [Threading.CancellationTokenSource]$cancellationSource ) {
    foreach( $channel in $channels ) {
        if( TestCancellation $cancellationSource ) { break }

        [Timestamp]$earliestSampleTime = GetEndSampleTime $Connection $channel
        if( $earliestSampleTime ) {
            Write-Verbose "⌚ $channel first recorded at $earliestSampleTime"
            $end = $earliestSampleTime

            # If server has repeatedly failed to return historic data earlier than current sample time, skip further retrieval attempts.
            [int]$callCount = GetRecordedExtentCount $Connection $channel $earliestSampleTime
            if( $callCount -gt 3 ) {
                Write-Verbose "Skipping retrieval of earlier samples after $callCount failed attempts"
                continue
            }
        }
        else {
            Write-Verbose "⌚ $channel has no recorded samples"

            # Use earliest start time of any other data requests, if present.
            $otherChannelRequests = GetRequestedDataEvents | Where-Object { $_.Channel -eq $channel }
            [Timestamp]$earliestRequestTime = $otherChannelRequests.Start | Sort-Object | Select-Object -First 1
            if( $earliestRequestTime -and $earliestRequestTime -eq [Timestamp]::MinValue ) { continue } # (skip if another "earliest data" request already exists)

            $end = ($earliestRequestTime ?? (Get-Date -AsUTC)).AddSeconds( -1 )
        }

        [bool]$added = AddRequestedDataEvent @{
            Channel = $channel
            End     = $end
        } "retrieving $channel samples before '$end'" ([Timestamp]::MinValue) $end -passthru

        if( $added ) { SetEarliestRecordedExtent $Connection $channel $earliestSampleTime }
    }
}

[CmdletBinding]
function RequestGaps( [InfluxConnection]$connection, [VueChannel[]]$channels, [Threading.CancellationTokenSource]$cancellationSource ) {
    foreach( $channel in $channels ) {
        if( TestCancellation $cancellationSource ) { break }

        $gapFreeStart, $gapFreeStop = GetGapFreeRange $connection $channel
        if( $gapFreeStart -ne $gapFreeStop ) {
            Write-Verbose "Checking for gaps in $channel outside of gap-free range between '$gapFreeStart' and '$gapFreeStop'"
            $before = @(GetMissingSampleRanges $connection $channel -stop $gapFreeStart)
            $after = @(GetMissingSampleRanges $connection $channel -start $gapFreeStop)
            $gapRanges = $before + $after
        }
        else {
            Write-Verbose "Checking for gaps in $channel"
            $gapRanges = GetMissingSampleRanges $connection $channel
        }

        $gaps = @($gapRanges |
                ForEach-Object {
                    $start = $_.Start
                    $end = $_.End
                    $missing = $_.Missing
                    $description = "$missing missing $channel samples (from '$start' to '$end')"

                    AddRequestedDataEvent @{
                        Channel      = $channel
                        End          = $end
                        MinimumStart = $start
                    } "retrieving $description" $start $end

                    $description
                })
        if( $gaps.Count ) { Write-Verbose "$channel has $($gaps.Count) gaps: $($gaps -join ', ')" }

        $dataStart, $dataStop = GetRecordedExtents $connection $channel -earliest -latest
        if( $dataStart -ne $dataStop ) {
            $newFreeStart = $dataStart
            if( $gaps.Count -eq 0 ) {
                $newFreeStop = $dataStop
            }
            else {
                $firstGapStart = GetRequestedDataEvents |
                    Where-Object Channel -EQ $channel |
                    Where-Object Description -Match ' missing .* samples ' |
                    Measure-Object Start -Minimum
                $newFreeStop = $firstGapStart ? $firstGapStart.Minimum.AddSeconds( -1 ) : $newFreeStart
            }

            $oldExtent = $gapFreeStop.DateTime - $gapFreeStart.DateTime
            $newExtent = $newFreeStop.DateTime - $newFreeStart.DateTime
            if( $newExtent -gt $oldExtent ) {
                RecordGapFreeRange $connection $channel $newFreeStart $newFreeStop
            }
        }
    }
}

[CmdletBinding]
function ProcessRequest( [InfluxConnection]$connection, [RequestedData]$requested, [Threading.CancellationTokenSource]$cancellationSource ) {
    $requestArgs = $requested.RequestArgs
    $end = $requested.End
    $channel = $requested.Channel

    $recorded = RecordRequestedData $connection $requestArgs $cancellationSource
    if( $recorded ) { Write-Verbose "✏ Recorded $recorded samples of energy usage data for $channel to Influx, up to $end" }
    else { Write-Warning "Could not retrieve any energy usage data for $channel up to $end" }
}


# "operate with 2k requests per day"  – TedGrahamEmporia (https://github.com/magico13/PyEmVue/issues/19#issuecomment-861834170)
[int]$script:DailyRequestLimit = 2000
[int]$script:HourlyRequestLimit = $script:DailyRequestLimit / 24

[TimeSpan]$script:OldDataRetrievalLimit = [TimeSpan]::FromDays( 99 )

[int]$script:EscapeKeyCode = 27

[string]$script:UsageMeasurement = 'energy_usage'
[string]$script:UsageField = 'usage'

[string]$script:ApiMeasurement = 'api_calls'
[string]$script:ApiField = 'status'

[string]$script:AnalysisMeasurement = 'analysis_results'
[string]$script:AnalysisField = 'value'
[string]$script:ExtentKind = 'extent'
[string]$script:GapFreeKind = 'gap_free'

[string]$script:RequestedDataEventId = 'EmporiaRecorder.RequestedData'



[CmdletBinding]
function TestCancellation( [Threading.CancellationTokenSource]$cancellationSource ) {
    if( !$cancellationSource.IsCancellationRequested -and $Host.UI.RawUI.KeyAvailable ) {
        $key = $Host.UI.RawUI.ReadKey( [Management.Automation.Host.ReadKeyOptions]::IncludeKeyDown -bor [Management.Automation.Host.ReadKeyOptions]::IncludeKeyUp -bor [Management.Automation.Host.ReadKeyOptions]::NoEcho )
        if( $key.VirtualKeyCode -eq $script:EscapeKeyCode ) {
            Write-Information "🛑 Received 'Esc' cancellation request; processing will stop after current step completes . . ."
            $cancellationSource.Cancel()
        }
    }

    $cancellationSource.IsCancellationRequested
}

[CmdletBinding]
function WaitUntil( [DateTime]$target, [Threading.CancellationTokenSource]$cancellationSource, [string]$taskMessage = 'before processing next event' ) {
    [TimeSpan]$remaining = $target - (Get-Date -AsUTC)
    [double]$totalSeconds = $remaining.TotalSeconds
    if( $totalSeconds -gt 1 ) {
        function GetWaitTimeMessage( [int]$seconds ) {
            [int]$minutes = [math]::Round( $seconds / 60.0 )
            if( $minutes -gt 1 ) {
                "⏳ Waiting $minutes minutes"
            }
            elseif( $minutes -eq 1 -and $totalSeconds -gt 60 ) {
                '⏳ Waiting 1 minute'
            }
            elseif( $seconds -gt 1 ) {
                "⏳ Waiting $seconds seconds"
            }
            else {
                '⏳ Waiting 1 second'
            }
        }

        $timeMessage = GetWaitTimeMessage $totalSeconds
        Write-Verbose "$timeMessage $taskMessage"
        while( ($now = Get-Date -AsUTC) -lt $target ) {
            if( TestCancellation $cancellationSource ) { break }

            $remainingSeconds = ($target - $now).TotalSeconds
            $timeMessage = GetWaitTimeMessage $remainingSeconds
            $percent = [math]::Min( 100, 100 * $remainingSeconds / $totalSeconds )
            Write-Progress $timeMessage $taskMessage -PercentComplete $percent

            Start-Sleep -Seconds 1
        }

        Write-Progress $timeMessage $taskMessage -Completed
    }
}


[CmdletBinding]
function GetBaseQuery( [InfluxConnection]$connection, [string]$range, [string]$condition ) {
    @'
from(bucket: "{0}")
    |> range({1})
    |> filter(fn: (r) =>
{2}
    )

'@ -f $connection.Bucket, $range, $condition
}

[CmdletBinding]
function GetBaseChannelQuery( [InfluxConnection]$connection, [VueChannel]$channel, [string]$range = 'start: 0' ) {
    GetBaseQuery $connection $range (@'
        r._measurement == "{0}" and
        r.device_gid == "{1}" and
        r.channel_number == "{2}" and
        r.channel_name == "{3}" and
        exists r._value
'@ -f $script:UsageMeasurement, $channel.DeviceGid, $channel.Number, $channel.Name)
}

[CmdletBinding]
function GetEndSampleTime( [InfluxConnection]$connection, [VueChannel]$channel, [switch]$last ) {
    $queryArgs = @{ connection = $connection; channel = $channel }
    if( $last ) {
        $endFunction = 'last'
        [Timestamp]$latest = GetRecordedExtents $connection $channel -latest
        if( $latest ) { $queryArgs['range'] = 'start: ' + $latest.AddHours( -1 ).ToQueryString() }
    }
    else {
        $endFunction = 'first'
        [Timestamp]$earliest = GetRecordedExtents $connection $channel -earliest
        if( $earliest ) { $queryArgs['range'] = 'start: 0, stop: ' + $earliest.AddHours( 1 ).ToQueryString() }
    }

    $query = (GetBaseChannelQuery @queryArgs) + @"
    |> $endFunction()
"@
    $endSamplesPerTable = Get-InfluxData $connection -Query $query
    $endSample = $endSamplesPerTable | Sort-Object Timestamp -Descending:$last | Select-Object -First 1

    [Timestamp]$endSample.Timestamp
}

[CmdletBinding]
function GetEarliestRecordedScale( [InfluxConnection]$connection, [VueChannel]$channel, [Timestamp]$timestamp ) {
    if( $timestamp ) {
        $range = 'start: ' + $timestamp.ToQueryString()
        $query = (GetBaseChannelQuery $connection $channel $range) + @'
    |> filter(fn: (r) => exists r.original_scale and r.original_scale != "Second")
    |> first()
'@
        $earliestWrites = Get-InfluxData $connection -Query $query
        $earliestSample = $earliestWrites | Sort-Object Timestamp | Select-Object -First 1
    }

    [Scale]($earliestSample.OriginalScale ?? [Scale]::Second)
}

[CmdletBinding]
function GetMissingSampleRanges( [InfluxConnection]$connection, [VueChannel]$channel, [Timestamp]$start = [Timestamp]::MinValue, [Timestamp]$stop = (Get-Date -AsUTC) ) {
    $range = 'start: {0}, stop: {1}' -f $start.ToQueryString(), $stop.ToQueryString()
    $query = (GetBaseChannelQuery $connection $channel $range) + @'
    |> drop(columns: ["original_scale"])
    |> sort(columns: ["_time"])
    |> elapsed()
    |> filter(fn: (r) => r.elapsed > 1)
'@
    $gaps = Get-InfluxData $connection -Query $query
    foreach( $gap in $gaps ) {
        [Timestamp]$timestamp = $gap.Timestamp
        [int]$missing = $gap.Elapsed - 1
        [PSCustomObject]@{
            Start   = $timestamp.AddSeconds( -$missing )
            End     = $timestamp
            Missing = $missing
        }
    }
}


[CmdletBinding]
function GetApiCallCountAndMinutesRemaining( [InfluxConnection]$connection, [double]$allowedMinutes ) {
    [Timestamp]$startHour = Get-Date -AsUTC -Minute 0 -Second 0 -Millisecond 0
    [Timestamp]$startDay = $startHour.AddHours( -23 )
    $range = 'start: ' + $startDay.ToQueryString()
    $query = GetBaseQuery $connection $range (@'
        r._measurement == "{0}"
'@ -f $script:ApiMeasurement)
    $apiCalls = Get-InfluxData $connection -Query $query

    $dayCallCount = $apiCalls.Count
    $hourCallCount = $apiCalls | Where-Object Timestamp -GE $startHour.Instant | Measure-Object | Select-Object -ExpandProperty Count
    $hourRemainingCount = [math]::Max( 0, $script:HourlyRequestLimit - $hourCallCount )
    $dayRemainingCount = [math]::Max( 0, $script:DailyRequestLimit - $dayCallCount )
    $remainingCount = [math]::Min( $hourRemainingCount, $dayRemainingCount )

    $remainingTime = (Get-Date -AsUTC) - $startHour.DateTime
    $remainingMinutes = [math]::Max( 1, [math]::Min( $allowedMinutes, [math]::Floor( 60 - $remainingTime.TotalMinutes ) ) )
    Write-Verbose "⚖ $remainingCount API calls remaining for $remainingMinutes minutes (used $hourCallCount of $script:HourlyRequestLimit per hour, $dayCallCount of $script:DailyRequestLimit per 24 hours)"

    $remainingCount, $remainingMinutes
}

[CmdletBinding]
function RecordApiCalls( [InfluxConnection]$connection ) {
    $apiCalls = Get-Event 'EmporiaEnergy.ApiCall' -ErrorAction SilentlyContinue
    if( !$apiCalls ) { return }

    Write-Verbose "📝 Recording $($apiCalls.Count) Emporia API calls"
    $apiData = @($apiCalls | ForEach-Object {
            $call, $status = $_.SourceArgs
            $timestamp = $_.TimeGenerated
            [PSCustomObject]@{
                Call      = $call
                Timestamp = $timestamp
                Value     = [int]$status
            }
        } | Group-Object Call | ForEach-Object {
            [SampleData]@{
                Tags    = @{ call = $_.Name }
                Samples = $_.Group
            }
        })
    Add-InfluxData $connection -Data $apiData -Measurement $script:ApiMeasurement -Field $script:ApiField

    $apiCalls | Remove-Event
}


[CmdletBinding]
function AddChannelTags( [VueChannel]$channel, [hashtable]$tags = @{} ) {
    $tags['channel_name'] = $channel.Name
    $tags['channel_number'] = $channel.Number
    $tags['device_gid'] = $channel.DeviceGid
    $tags
}


[CmdletBinding]
function GetAnalysisDataCondition( [VueChannel]$channel, [string]$kind, [switch]$predicate ) {
    $condition = (@'
        r._measurement == "{0}" and
        r.device_gid == "{1}" and
        r.channel_number == "{2}" and
        r.channel_name == "{3}" and
        r.kind == "{4}"
'@ -f $script:AnalysisMeasurement, $channel.DeviceGid, $channel.Number, $channel.Name, $kind)
    if( $predicate ) {
        $predicateSyntax = $condition.Replace( ' r.', '' ).Replace( ' == ', '=' )
        $predicateSyntax.Split( "`n", [StringSplitOptions]::RemoveEmptyEntries -bor [StringSplitOptions]::TrimEntries ) -join ' '
    }
    else {
        $condition
    }
}

[CmdletBinding]
function GetAnalysisData( [InfluxConnection]$connection, [VueChannel]$channel, [string]$kind, [string]$range = 'start: 0' ) {
    $query = GetBaseQuery $connection $range (GetAnalysisDataCondition $channel $kind)
    Get-InfluxData $connection -Query $query
}

[CmdletBinding]
function RecordAnalysisData {
    param(
        [Parameter( Mandatory )][InfluxConnection]$connection,
        [Parameter( Mandatory )][VueChannel]$channel,
        [Parameter( Mandatory )][hashtable]$tags,
        [Parameter( Mandatory, ValueFromPipeline = $true )][SampleData[]]$data
    )

    begin { $allData = @() }
    process { $allData += $data }
    end {
        $tags = AddChannelTags $channel $tags
        Add-InfluxData $connection -Data $allData -Measurement $script:AnalysisMeasurement -Field $script:AnalysisField -Tags $tags
    }
}


[CmdletBinding]
function GetRecordedExtents( [InfluxConnection]$connection, [VueChannel]$channel, [switch]$earliest, [switch]$latest, [switch]$raw ) {
    $data = GetAnalysisData $connection $channel $script:ExtentKind | Sort-Object Timestamp

    if( $raw -and !$earliest -and !$latest ) { $data }
    else {
        $values = @()
        if( $earliest ) { $values += $data[0] }
        if( $latest ) { $values += $data[-1] }

        if( $raw ) { $values }
        else { [Timestamp[]]$values.Timestamp }
    }
}

[CmdletBinding]
function GetRecordedExtentCount( [InfluxConnection]$connection, [VueChannel]$channel, [Timestamp]$timestamp ) {
    $extents = GetRecordedExtents $connection $channel -raw
    $count = $extents | Where-Object Timestamp -EQ $timestamp.Instant | Select-Object -ExpandProperty Value
    $count ?? 0
}

[CmdletBinding]
function SetLatestRecordedExtent( [InfluxConnection]$connection, [VueChannel]$channel, [Timestamp]$timestamp ) {
    $latest = GetRecordedExtents $connection $channel -latest
    if( $timestamp -ge $latest ) {
        SetRecordedExtent $connection $channel 'latest' $timestamp $latest -deleteExisting
    }
}

[CmdletBinding]
function SetEarliestRecordedExtent( [InfluxConnection]$connection, [VueChannel]$channel, [Timestamp]$timestamp ) {
    $earliest, $latest = GetRecordedExtents $connection $channel -earliest -latest
    if( $timestamp -le $earliest ) {
        $multipleExtents = $earliest -ne $latest
        SetRecordedExtent $connection $channel 'earliest' $timestamp $earliest -deleteExisting:$multipleExtents
    }
}

[CmdletBinding]
function SetRecordedExtent(
    [InfluxConnection]$connection,
    [VueChannel]$channel,
    [string]$kind,
    [Timestamp]$newTime,
    [Timestamp]$currentTime,
    [switch]$deleteExisting
) {
    if( $timestamp ) {
        if( $timestamp -eq $currentTime ) {
            $currentCount = GetRecordedExtentCount $connection $channel $currentTime
            Write-Verbose "Updating $kind recorded extent for $channel at '$timestamp' ($currentCount)"
        }
        else {
            $currentCount = 0
            Write-Verbose "Setting $kind recorded extent for $channel to '$timestamp'"

            if( $currentTime -and $deleteExisting ) {
                $predicate = GetAnalysisDataCondition $channel $script:ExtentKind -predicate
                Remove-InfluxData $connection -Start $currentTime.DateTime -End $currentTime.DateTime -Predicate $predicate
            }
        }

        RecordAnalysisData $connection $channel -tags @{ kind = $script:ExtentKind } -data @([SampleData]@{
                Timestamp = $timestamp
                Samples   = @(1 + $currentCount)
            })
    }
}


[CmdletBinding]
function GetGapFreeRange( [InfluxConnection]$connection, [VueChannel]$channel ) {
    $data = GetAnalysisData $connection $channel $script:GapFreeKind
    if( $data.Count -eq 2 ) {
        [Timestamp[]]$data.Timestamp | Sort-Object
    }
}

[CmdletBinding]
function RecordGapFreeRange( [InfluxConnection]$connection, [VueChannel]$channel, [Timestamp]$start, [Timestamp]$stop ) {
    # Ignore invalid ranges.
    if( $stop -le $start ) { return }

    Write-Verbose "Setting gap-free range for $channel between '$start' and '$stop'"

    # Remove any existing recorded range.
    $predicate = GetAnalysisDataCondition $channel $script:GapFreeKind -predicate
    Remove-InfluxData $connection -Start $start.DateTime -End $stop.DateTime -Predicate $predicate

    $start, $stop | ForEach-Object {
        [SampleData]@{
            Timestamp = $_
            Samples   = @(0)
        }
    } | RecordAnalysisData $connection $channel -tags @{ kind = $script:GapFreeKind }
}


class RequestedData {
    [string]$Description
    [Timestamp]$Start
    [Timestamp]$End
    [hashtable]$RequestArgs
    [VueChannel]$Channel

    [System.Management.Automation.PSEventArgs]$Event = $null
    [int]$EventIdentifier = -1

    RequestedData( [hashtable]$requestArgs, [string]$description, [Timestamp]$start, [Timestamp]$end ) {
        $this.Description = $description
        $this.Start = $start
        $this.End = $end
        $this.RequestArgs = $requestArgs
        $this.Channel = $requestArgs.Channel
    }

    [string] ToString() { return $this.Description }
}

[CmdletBinding]
function PurgeOldRequestedDataEvents {
    # Purge incompatible requests from old runs.
    $old = Get-Event -SourceIdentifier $script:RequestedDataEventId -ErrorAction SilentlyContinue |
        Where-Object { $_.SourceArgs -isnot [RequestedData] }
    if( $old.Count ) {
        Write-Verbose "Purging $($old.Count) obsolete data requests"
        $old | Remove-Event
    }
}

[CmdletBinding]
function GetRequestedDataEventCount {
    [OutputType([int])] param( )

    $requests = Get-Event -SourceIdentifier $script:RequestedDataEventId -ErrorAction SilentlyContinue | Measure-Object
    $requests.Count
}

[CmdletBinding]
function GetRequestedDataEvents {
    [OutputType([RequestedData[]])] param( [switch]$sort )

    $requests = Get-Event -SourceIdentifier $script:RequestedDataEventId -ErrorAction SilentlyContinue |
        Select-Object -ExpandProperty SourceArgs

    if( $sort ) { $requests | Sort-Object End -Descending }
    else { $requests }
}

[CmdletBinding]
function GetNextRequestedDataEvent {
    [OutputType([RequestedData])] param( [Timestamp]$before )

    $requested = @(GetRequestedDataEvents -sort)
    if( $before ) { $requested = @($requested | Where-Object End -LT $before) }
    $requested | Select-Object -First 1
}

[CmdletBinding]
function AddRequestedDataEvent( [hashtable]$requestArgs, [string]$description, [Timestamp]$start, [Timestamp]$end, [switch]$passthru ) {
    $existing = GetRequestedDataEvents | Where-Object Description -EQ $description
    if( !$existing ) {
        $request = [RequestedData]::new( $requestArgs, $description, $start, $end )
        $request.Event = New-Event -SourceIdentifier $script:RequestedDataEventId -Sender $MyInvocation.MyCommand.Module -EventArguments $request
        $request.EventIdentifier = $request.Event.EventIdentifier
        if( $passthru ) { $request }
    }
}


[CmdletBinding]
function RecordRequestedData( [InfluxConnection]$connection, [hashtable]$requestArgs, [Threading.CancellationTokenSource]$cancellationSource ) {
    $channel = $requestArgs.Channel
    $end = $requestArgs.End
    if( $end ) {
        # Start querying with the most recent Scale value that was successful at this time range, to avoid known-unsuccessful API calls.
        $earliestBacklogScale = GetEarliestRecordedScale $connection $channel $end
        $scales = @([Scale]::Second, [Scale]::Minute, [Scale]::Hour) | Where-Object { $_ -ge $earliestBacklogScale }
    }
    else {
        $scales = @([Scale]::Second)
    }

    foreach( $scale in $scales ) {
        if( TestCancellation $cancellationSource ) { break }

        [SampleData[]]$data = @()
        $charts = Get-VueChartUsage @requestArgs -Scale $scale
        foreach( $chart in $charts ) {
            if( TestCancellation $cancellationSource ) { break }

            [Timestamp]$timestamp = $chart.Timestamp
            $usage = $chart.UsageList

            $force = $false
            $validCount = $usage | Where-Object { [double]::IsFinite( $_ ) } | Measure-Object | ForEach-Object Count
            Write-Verbose "Received $validCount valid out of $($usage.Count) total $scale samples for $channel at '$timestamp'"
            if( !$validCount ) {
                $age = (Get-Date -AsUTC) - $timestamp.DateTime
                if( $scale -eq [Scale]::Hour -and $age -gt $script:OldDataRetrievalLimit ) {
                    Write-Verbose "- Ending checks for missing data over $([int]($age.TotalHours / 24)) days old"
                    $force = $true
                }
                else {
                    continue
                }
            }

            if( $scale -eq [Scale]::Second ) {
                $data += [SampleData]@{
                    Timestamp = $timestamp
                    Samples   = $usage
                }
            }
            else {
                # Expand chart data values to match Second-resolution samples of latest data available.
                [DateTime]$offset = &$chart.Delta $timestamp.DateTime
                $scaleFactor = ($offset - $timestamp.DateTime).TotalSeconds

                [Timestamp]$nextTimestamp = $timestamp
                for( $i = 0; $i -lt $usage.Count; $i += 1 ) {
                    if( TestCancellation $cancellationSource ) { break }

                    $timestamp = $nextTimestamp
                    $nextTimestamp = $nextTimestamp.AddSeconds( $scaleFactor )

                    $sample = $usage[$i]
                    if( ![double]::IsFinite( $sample ) ) {
                        if( !$force -or $i -ge 100 ) { continue }

                        $sample = -1.0
                    }

                    $expanded = @($sample) * $scaleFactor

                    # Adjust expanded data to match target range, rather than Scale-rounded API results:
                    if( $i -eq 0 -and $requestArgs.MinimumStart ) {
                        [DateTime]$missedStart = $requestArgs.MinimumStart.DateTime
                        [int]$overshoot = ($missedStart - $timestamp.DateTime).TotalSeconds
                        # - Skip starting data we have already recorded.
                        if( $overshoot -gt 0 ) {
                            $adjustments += @("aligned $overshoot")
                            $expanded = $expanded[$overshoot..($expanded.Count - 1)]
                            $timestamp = $missedStart
                        }
                    }
                    elseif( $i + 1 -eq $usage.Count ) {
                        [int]$gap = ($end.Datetime - $nextTimestamp.DateTime).TotalSeconds
                        # - Skip ending data we have already recorded.
                        if( $gap -lt 0 ) {
                            $extra = - $gap
                            $adjustments += "capped $extra"
                            [Array]::Resize( [ref]$expanded, $expanded.Count - $extra )
                        }
                        # - Pad ending data that did not round up to the end Scale resolution.
                        elseif( $gap -gt 0 -and $gap -le $scaleFactor ) {
                            $adjustments += "padded $gap"
                            $expanded += @($expanded[-1]) * $gap
                        }
                    }

                    $data += [SampleData]@{
                        Timestamp = $timestamp
                        Samples   = $expanded
                    }
                }

                $adjustmentMessage = $adjustments ? " ($($adjustments -join ', '))" : $null
                $startTimestamp = $data[0].Timestamp
                if( $startTimestamp -and $chart.Timestamp -ne $startTimestamp.DateTime ) { $adjustmentMessage = " at '$startTimestamp'$adjustmentMessage" }

                $sampleCount = $data.Samples.Count | Measure-Object -Sum | ForEach-Object Sum
                Write-Verbose "Expanded $($usage.Count) $($chart.Scale) samples at '$($chart.Timestamp.ToLocalTime())' to $sampleCount Second samples$adjustmentMessage"

                # Write expanded samples from latest to earliest.
                [array]::Reverse( $data )
            }
        }

        if( TestCancellation $cancellationSource ) { break }
        [int]$recorded = RecordChartData $connection $chart $data

        # Stop as soon as we successfully record data.
        if( $recorded ) { return $recorded }
    }
}

[CmdletBinding]
function RecordChartData( [InfluxConnection]$connection, [VueChartUsage]$chart, [SampleData[]]$data ) {
    if( $data.Count ) {
        $tags = AddChannelTags $chart.Channel @{
            original_scale = $chart.Scale
            unit           = $chart.Unit
        }
        Add-InfluxData $connection -Data $data -Measurement $script:UsageMeasurement -Field $script:UsageField -Tags $tags -Count
    }
}

<#
PowerShellGet v3:
- https://devblogs.microsoft.com/powershell/powershellget-3-0-preview-11-release/
- Register-PSResourceRepository -Name NuGet -URL https://api.nuget.org/v3/index.json
  - https://docs.microsoft.com/en-us/nuget/nuget-org/overview-nuget-org#api-endpoint-for-nugetorg
- Find-PSResource Amazon.Extensions.CognitoAuthentication
- Not working in v3.0.11:
  - Install-PSResource Amazon.Extensions.CognitoAuthentication -Repository NuGet
  - Save-PSResource -Repository NuGet -Name Amazon.Extensions.CognitoAuthentication -verbose
#>
