﻿#requires -Version 5

if( !$DefaultImportNuGetRuntime ) {
    $global:DefaultImportNuGetRuntime = 'netstandard'
}

function Import-NuGet {
    <#
    .Synopsis
        Loads the assemblies for a NuGet package previously setup by Install-Package.

    .Description
        Searches for the latest version of a named NuGet package, and uses Add-Type to load all of the NuGet's assemblies.

        Use `Install-Package` with the NuGet package provider (`Install-PackageProvider Nuget`) to add NuGet packages for import.

    .Notes
        Disclaimer: This project is not affiliated with or endorsed by Microsoft.

    .Link
        Install-Package
    .Link
        Add-Type
    #>
    [CmdletBinding()]
    param(
        # The name of the NuGet package to import.
        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [Alias('Name')]
        [string]$PackageName
        ,
        # The .NET Runtime to search for in the NuGet, such as 'netstandard'.
        [string]$Runtime = $($DefaultImportNuGetRuntime)
        ,
        # See `Get-Help Add-Type -Parameter PassThru`.
        [switch]$PassThru
    )

    process {
        $package = Get-Package -Name $PackageName
        $directory = Split-Path -Parent $package.Source
        Write-Verbose "Searching for $PackageName assemblies in $directory."
        $library = Join-Path $directory 'lib' |
            Get-ChildItem -Filter "${Runtime}*" |
            Sort-Object Name |
            Select-Object -Last 1 -ExpandProperty FullName

        $assemblies = @(Get-ChildItem $library -Filter *.dll)
        if( !$assemblies ) { throw "Could not find any '$Runtime' assemblies to load for NuGet package '$PackageName'." }

        $appDomain = [AppDomain]::CurrentDomain
        $assemblyResolveEvent = [AppDomain].GetEvent( 'AssemblyResolve' )
        [ResolveEventHandler]$loadedAssemblyResolver = {
            param( [object]$s, [ResolveEventArgs]$e )

            Write-Verbose "Searching for loaded assembly matching '$($e.Name)'"
            [Reflection.AssemblyName]$target = $e.Name
            $match = $appDomain.GetAssemblies() | Where-Object {
                [Reflection.AssemblyName]$assembly = $_.FullName
                $assembly.Name -eq $target.Name
            } | Select-Object -First 1

            if( $match ) { Write-Verbose "Found loaded assembly '$($match.FullName)'" }
            return $match
        }

        $assemblyResolveEvent.AddMethod.Invoke( $appDomain, $loadedAssemblyResolver )
        try {
            $assemblies | ForEach-Object {
                Write-Verbose "Loading $($_.FullName)"
                Add-Type -LiteralPath $_.FullName -PassThru:$PassThru
            }
        }
        catch [System.IO.FileLoadException], [System.Reflection.ReflectionTypeLoadException] {
            $exception = $_.Exception
            Write-Warning "[$($exception.GetType().FullName)] $exception"
            $uniqueMessages = [System.Collections.Generic.HashSet[string]]::new()
            $exception.LoaderExceptions | Where-Object { $_ -and $uniqueMessages.Add( $_.Message ) } | Write-Warning
            throw
        }
        finally {
            $assemblyResolveEvent.RemoveMethod.Invoke( $appDomain, $loadedAssemblyResolver )
        }
    }
}
