﻿#requires -Version 5


function Start-InfluxDaemon {
    <#
    .Synopsis
        Starts the Influx daemon process in a background job.

    .Description
        Use `Start-InfluxDaemon` to start running the influxd.exe daemon process in a background job managed by the module.

        Use `Stop-InfluxDaemon` to stop running the influxd.exe daemon process managed by the module.

    .Notes
        This module requires the `InfluxDB.Client` NuGet package (`Install-Package InfluxDB.Client`).

        Disclaimer: This project is not affiliated with or endorsed by InfluxDB.

    .Link
        https://github.com/influxdata/influxdb-client-csharp
    #>
    [CmdletBinding(DefaultParameterSetName = 'Default')]
    [OutputType([System.Management.Automation.Job])]
    param(
        # Indicates the comand should not return until `Test-Influx` succeeds for the specified connection information.
        [Parameter(Mandatory, Position = 0, ValueFromPipeline, ValueFromPipelineByPropertyName, ParameterSetName = 'Wait')]
        [InfluxConnection]$Connection
        ,
        # The direct path to the `influxd.exe` daemon program, or the path to the Influx installation to search for `influxd.exe`.
        [Parameter(Position = 0, ValueFromPipelineByPropertyName, ParameterSetName = 'Default')]
        [Parameter(Position = 1, ValueFromPipelineByPropertyName, ParameterSetName = 'Wait')]
        [string]$Path = $(Join-Path $env:ProgramFiles 'InfluxData')
        ,
        # Forces any existing background job to stop before starting the Influx daemon process.
        [switch]$Force
        ,
        # Returns the background job running the Influx daemon process. By default, this cmdlet does not generate any output.
        [switch]$PassThru
    )

    process {
        GetInfluxExe ([ref]$Path) 'influxd.exe'

        if( $Force ) {
            Write-Verbose 'Forcing any existing module Influx Daemon background job to stop'
            Stop-InfluxDaemon
        }
        else {
            $existing = GetInfluxDaemon
            if( $existing -and $existing.JobStateInfo.State -ne [System.Management.Automation.JobState]::Running ) {
                Write-Verbose "Removing existing module Influx Daemon background job that is $($existing.JobStateInfo.State)"
                Stop-InfluxDaemon
            }
        }

        Write-Verbose "Starting module Influx Daemon background job with path '$Path'"
        $job = Start-Job -Name $script:InfluxDaemonJobName -ScriptBlock { &$using:Path }

        if( $Connection ) {
            Write-Verbose "Waiting for Influx Daemon to successfully respond to $Connection connection request"
            while( !(Test-Influx $Connection) ) {
                Start-Sleep 0.25
            }
        }

        if( $PassThru ) {
            $job
        }
    }
}

function Stop-InfluxDaemon {
    <#
    .Synopsis
        Stops the Influx daemon process in a background job.

    .Description
        Use `Stop-InfluxDaemon` to stop running the influxd.exe daemon process managed by the module.

        Use `Start-InfluxDaemon` to start running the influxd.exe daemon process in a background job managed by the module.

    .Notes
        This module requires the `InfluxDB.Client` NuGet package (`Install-Package InfluxDB.Client`).

        Disclaimer: This project is not affiliated with or endorsed by InfluxDB.

    .Link
        https://github.com/influxdata/influxdb-client-csharp
    #>
    [CmdletBinding()]
    param(
    )

    process {
        GetInfluxDaemon |
            ForEach-Object { Write-Verbose "Stopping $($_.Name) ($($_.Id))"; $_ } |
            Stop-Job -PassThru |
            Remove-Job
    }
}


function New-InfluxConnection {
    <#
    .Synopsis
        Creates an object used to store the connection information used to connect to InfluxDB.

    .Description
        `InfluxConnection` holds the Influx connection information used by the read and write commands in this module.

        Use `Test-Influx` to check whether a given connection is healthy.

        Use `Get-InfluxData` to query data from Influx.

        Use `Add-InfluxData` to send data to Influx.

    .Notes
        This module requires the `InfluxDB.Client` NuGet package (`Install-Package InfluxDB.Client`).

        Disclaimer: This project is not affiliated with or endorsed by InfluxDB.

    .Link
        https://github.com/influxdata/influxdb-client-csharp
    #>
    [OutputType([InfluxConnection])]
    param(
        # The token used to authenticate with InfluxDB.
        [Parameter(Mandatory, Position = 0, ValueFromPipelineByPropertyName)]
        [string]$AuthenticateToken
        ,
        # The Influx organization containing the desired data.
        [Parameter(Position = 1, ValueFromPipelineByPropertyName)]
        [string]$Organization
        ,
        # The Influx bucket containing the desired data.
        [Parameter(Position = 2, ValueFromPipelineByPropertyName)]
        [string]$Bucket
        ,
        # The address of the Influx instance.
        [Parameter(Position = 3, ValueFromPipelineByPropertyName)]
        [string]$Url = 'http://localhost:8086/'
        ,
        # The timeout for Influx interactions.
        [Parameter(Position = 4, ValueFromPipelineByPropertyName)]
        [TimeSpan]$Timeout = [TimeSpan]::FromMilliseconds( -1 )
    )

    process {
        [InfluxConnection]@{
            AuthenticateToken = $AuthenticateToken
            Organization      = $Organization
            Bucket            = $Bucket
            Url               = $Url
            Timeout           = $Timeout
        }
    }
}

function Test-Influx {
    <#
    .Synopsis
        Tests whether Influx is running and accessible.

    .Description
        Issues a Health Check query to test whether Influx is running and accessible at the specified connection.

        Use `New-InfluxConnection` to create the Influx connection information.

        Use `Get-InfluxData` to query data from Influx.

        Use `Add-InfluxData` to send data to Influx.

    .Notes
        This module requires the `InfluxDB.Client` NuGet package (`Install-Package InfluxDB.Client`).

        Disclaimer: This project is not affiliated with or endorsed by InfluxDB.

    .Link
        https://github.com/influxdata/influxdb-client-csharp
    #>
    [CmdletBinding(DefaultParameterSetName = 'BooleanResult')]
    [OutputType([bool], ParameterSetName = 'BooleanResult')]
    [OutputType([InfluxDB.Client.Api.Domain.HealthCheck], ParameterSetName = 'FullResult')]
    param(
        # Influx connection information.
        [Parameter(Mandatory, Position = 0, ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [InfluxConnection]$Connection
        ,
        # Returns the full `HealthCheck` result returned by InfluxDB. By default, this cmdlet returns a Boolean result.
        [Parameter(ParameterSetName = 'FullResult')]
        [switch]$FullResult
    )

    process {
        $client = $Connection.GetDefaultClient()
        $task = $client.HealthAsync()
        $result = $task.Result
        if( $FullResult ) {
            $result
        }
        else {
            $result.Status -eq 'Pass'
        }
    }
}

function Get-InfluxData {
    <#
    .Synopsis
        Reads data from Influx.

    .Description
        Sends a Flux query to Influx at the specified connection.

        Use `New-InfluxConnection` to create the Influx connection information.

        Use `Test-Influx` to check whether a given connection is healthy.

        Use `Add-InfluxData` to send data to Influx.

        Use `Remove-InfluxData` to remove data from Influx.

    .Notes
        This module requires the `InfluxDB.Client` NuGet package (`Install-Package InfluxDB.Client`).

        Disclaimer: This project is not affiliated with or endorsed by InfluxDB.

    .Link
        https://github.com/influxdata/influxdb-client-csharp
    #>
    [CmdletBinding()]
    [OutputType([object])]
    param(
        # Influx connection information.
        [Parameter(Mandatory, Position = 0, ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [InfluxConnection]$Connection
        ,
        # The Flux query for Influx to process. If no query is provided, a default query will pull all data in `$Connection.Bucket`.
        [Parameter(Position = 1, ValueFromPipelineByPropertyName)]
        [string]$Query = $null
        ,
        # A flag to automatically call `Start-InfluxDaemon` if `Test-Influx` returns false.
        [switch]$AutoStart
    )

    process {
        AutoStartInfluxDaemon $AutoStart $Connection

        if( -not $Query ) { $Query = 'range(start: 0)' }
        if( $Query -notmatch '^from' ) { $Query = 'from(bucket: "{0}") |> {1}' -f $Connection.Bucket, $Query }

        $client = $Connection.GetDefaultClient()
        $queryApi = $client.GetQueryApi()
        if( $null -eq $queryApi ) { throw "Could not retrieve Query API for connection: $Connection" }

        [string[]]$queryLines = $Query.Split( [Environment]::NewLine.ToCharArray(), [StringSplitOptions]::RemoveEmptyEntries )
        [string]$queryDisplay = $queryLines.Count -gt $FormatEnumerationLimit ? $queryLines.Trim() -join ' ' : "`n  " + $Query.Trim()
        $result, $elapsed = @('Sending', 'Retrying') | ForEach-Object {
            try {
                Write-Verbose "$_ Influx async query: $queryDisplay"
                $r = $queryApi.QueryAsync( $Query )
                if( $null -eq $r ) { throw "Could not perform query: null task returned $r" }
                $e = Measure-Command { $r.Wait() }
                return $r, $e
            }
            catch [System.Management.Automation.MethodInvocationException] {
                Write-Verbose "Influx query failed: $_"
                for( $innerException = $_.InnerException; $innerException; $innerException = $_.InnerException ) {
                    Write-Verbose "- $innerException"
                }
            }
        } | Select-Object -First 2
        $records = $result.Result.Records ?? @()

        Write-Verbose "[$($elapsed.TotalSeconds.ToString('0.###'))s] Processing $($records.Count) records"
        $textInfo = [Globalization.CultureInfo]::CurrentCulture.TextInfo
        $results = @($records | ForEach-Object {
                $values = @{
                    RawSource = $result
                    RawObject = $_
                }
                foreach( $item in $_.Values.GetEnumerator() ) {
                    switch ($item.Key) {
                        'table' {}
                        'result' {}
                        '_start' {}
                        '_stop' {}

                        '_time' { $values['Timestamp'] = $item.Value }
                        default {
                            $key = $textInfo.ToTitleCase( $item.Key.Replace( '_', ' ' ).Trim() ).Replace( ' ', '' )
                            $values[$key] = $item.Value
                        }
                    }
                }

                [PSCustomObject]$values
            })
        Add-Member -InputObject $results -MemberType NoteProperty -Name RawSource -Value $result -Force -PassThru
    }
}

function Add-InfluxData {
    <#
    .Synopsis
        Writes data to Influx.

    .Description
        Sends a series of `WritePoints` to Influx for each set of samples in the specified collection of data items.

        Use `New-InfluxConnection` to create the Influx connection information.

        Use `Test-Influx` to check whether a given connection is healthy.

        Use `Get-InfluxData` to query data from Influx.

        Use `Remove-InfluxData` to remove data from Influx.

    .Notes
        This module requires the `InfluxDB.Client` NuGet package (`Install-Package InfluxDB.Client`).

        Disclaimer: This project is not affiliated with or endorsed by InfluxDB.

    .Link
        https://github.com/influxdata/influxdb-client-csharp
    .Link
        Module inspired by https://github.com/jertel/vuegraf
    #>
    [CmdletBinding(DefaultParameterSetName = 'Data')]
    [OutputType([int])]
    param(
        # Influx connection information.
        [Parameter(Mandatory, Position = 0, ValueFromPipelineByPropertyName)]
        [InfluxConnection]$Connection
        ,
        # The samples to store in Influx.  Each sample in `SampleData.Samples` should either be a simple value, or conform to the following schema:
        # ```
        # [sample]{
        #   [[hashtable]Tags]           # (optional) Individual sample tags
        #   [[string]Measurement]       # (optional) Measurement name (default is $Measurement)
        #   [[string]Field]             # (optional) Field name (default is $Field)
        #   [[Timestamp]Timestamp]      # (optional) Acquisition time (default is $Timestamp)
        #   [[WritePrecision]Precision] # (optional) Acquisition precision (default is [WritePrecision]::S)
        #   Value                       # Sample value (if property does not exist, sample object is used instead)
        # }
        # ```
        [Parameter(Mandatory, Position = 1, ValueFromPipeline, ValueFromPipelineByPropertyName, ParameterSetName = 'Data')]
        [Parameter(Mandatory, Position = 1, ValueFromPipeline, ValueFromPipelineByPropertyName, ParameterSetName = 'DataWithTimestamp')]
        [SampleData[]]$Data
        ,
        # The samples to store in Influx.  Each sample should either be a simple value, or conform to the following schema:
        # ```
        # [sample]{
        #   [[hashtable]Tags]           # (optional) Individual sample tags
        #   [[string]Measurement]       # (optional) Measurement name (default is $Measurement)
        #   [[string]Field]             # (optional) Field name (default is $Field)
        #   [[Timestamp]Timestamp]      # (optional) Acquisition time (default is $Timestamp)
        #   [[WritePrecision]Precision] # (optional) Acquisition precision (default is [WritePrecision]::S)
        #   Value                       # Sample value (if property does not exist, sample object is used instead)
        # }
        # ```
        [Parameter(Mandatory, Position = 1, ValueFromPipeline, ValueFromPipelineByPropertyName, ParameterSetName = 'Samples')]
        [Parameter(Mandatory, Position = 1, ValueFromPipeline, ValueFromPipelineByPropertyName, ParameterSetName = 'SamplesWithTimestamp')]
        [array]$Samples
        ,
        # The default Influx Measurement value for data points (if not specified on Samples).
        [Parameter(Position = 2, ValueFromPipelineByPropertyName)]
        [string]$Measurement = 'values'
        ,
        # The default Influx Field value for data points (if not specified on Samples).
        [Parameter(Position = 3, ValueFromPipelineByPropertyName)]
        [string]$Field = 'value'
        ,
        # The default Influx starting Timestamp value for data points (if not specified on Samples).
        [Parameter(Mandatory, Position = 4, ValueFromPipelineByPropertyName, ParameterSetName = 'DataWithTimestamp')]
        [Parameter(Mandatory, Position = 4, ValueFromPipelineByPropertyName, ParameterSetName = 'SamplesWithTimestamp')]
        [Timestamp]$Timestamp
        ,
        # Default tags for all samples.
        [Parameter(Position = 5, ValueFromPipelineByPropertyName)]
        [hashtable]$Tags
        ,
        # A flag to automatically call `Start-InfluxDaemon` if `Test-Influx` returns false.
        [switch]$AutoStart
        ,
        # Returns the number of points written to Influx. By default, this cmdlet does not generate any output.
        [switch]$Count
    )

    begin {
        AutoStartInfluxDaemon $AutoStart $Connection

        if( $Samples.Count ) { $Data = [SampleData]@{ Samples = $Samples } }

        [Timestamp]$nextTimestamp = $null
        if( $PSBoundParameters.ContainsKey( 'Timestamp' ) ) { $nextTimestamp = $Timestamp }

        $DefaultPrecision = [InfluxDB.Client.Api.Domain.WritePrecision]::S
        $optionsBuilder = AddTags $Connection.CreateOptionsBuilder() $Tags 'default'
        $options = $optionsBuilder.Build()

        $client = $Connection.CreateClient( $options )
        $writeApi = $client.GetWriteApi()
        Unregister-Event $script:WriteApiEventId -Force -ErrorAction SilentlyContinue
        Register-ObjectEvent $writeApi EventHandler -SourceIdentifier $script:WriteApiEventId -SupportEvent
        $writeCount = 0
    }

    process {
        # https://github.com/influxdata/influxdb-client-csharp/tree/master/Client#by-data-point
        [int]$dataCount = $Data.Count
        [int]$total = $Data.Samples.Count | Measure-Object -Sum | ForEach-Object Sum
        Write-Verbose "Writing $dataCount data items ($total total samples)"
        $batch = [System.Collections.Generic.List[InfluxDB.Client.Writes.PointData]]::new( $script:WriteBatchSize )
        $invalidValues = [System.Collections.Generic.HashSet[string]]::new()
        [int]$invalidCount = 0
        [int]$processedCount = 0
        for( $i = 0; $i -lt $dataCount; $i += 1 ) {
            $item = $Data[$i]
            $activity = "Add-InfluxData ($($item.GetType().Name) $(1 + $i) of $dataCount)"
            $builder = [InfluxDB.Client.Writes.PointData]::Measurement( $item.Measurement ?? $Measurement )
            $builder = AddTags $builder $item.Tags 'data'

            $batch.Clear()
            $nextTimestamp = $item.Timestamp ?? $nextTimestamp
            $status = "Writing $($item.Samples.Count) of $total total samples"
            $item.Samples | ForEach-Object {
                $sample = AddTags $builder $_.Tags 'point'

                $field = $_.Field ?? $Field
                $value = $_.Value ?? $_
                $point = $sample.Field( $field, $value )
                if( $time = (TryConvertToTimestamp $_.Timestamp) ?? $nextTimestamp ) {
                    $precision = $_.Precision ?? $DefaultPrecision
                    $nextTimestamp = IncrementTime $time.Instant $precision
                    $point = $point.Timestamp( $time.Instant, $precision )
                }

                $processedCount += 1
                if( $processedCount % $script:WriteBatchSize -eq 0 ) {
                    $processedMessage = "$processedCount processed"
                    if( $invalidCount -eq 0 ) {
                        $info = $processedMessage
                    }
                    else {
                        $invalidMessage = "$invalidCount invalid: $($invalidValues -join ', ')"
                        $info = $invalidCount -eq $processedCount ? $invalidMessage : "$processedMessage; $invalidMessage"
                    }

                    $percent = [math]::Max( 100, 100 * $processedCount / $total )
                    Write-Progress $activity "$status ($info)" -PercentComplete $percent
                }

                # https://github.com/influxdata/influxdb-python/issues/422 - DataFrame write "nan", "inf" error in influxdb
                if( $value -is [double] -and ![double]::IsFinite( $value ) ) {
                    [void]$invalidValues.Add( "$value" )
                    $invalidCount += 1
                }
                elseif( $value -is [float] -and ![float]::IsFinite( $value ) ) {
                    [void]$invalidValues.Add( "$value" )
                    $invalidCount += 1
                }
                else {
                    $point
                }
            } | ForEach-Object -Process {
                $batch.Add( $_ )
                if( $batch.Count -eq $batch.Capacity ) {
                    ProcessWriteEvents
                    $writeCount += $batch.Count
                    $writeApi.WritePoints( $batch )
                    $batch.Clear()
                }
            } -End {
                ProcessWriteEvents
                if( $batch.Count ) {
                    $writeCount += $batch.Count
                    $writeApi.WritePoints( $batch )
                    $batch.Clear()
                }
            }

        }

        if( $invalidCount ) { Write-Verbose "Ignored $invalidCount invalid floating-point samples ($($invalidValues -join ', '))" }
        Write-Progress $activity $status -Completed
    }

    end {
        Write-Verbose "Flushing $writeCount point writes"
        $writeApi.Flush()

        if( $writeCount ) {
            Write-Verbose 'Waiting for writes to complete'
            Wait-Event $script:WriteApiEventId | Out-Null
        }

        $writeApi.Dispose()
        Unregister-Event $script:WriteApiEventId -Force

        ProcessWriteEvents

        if( $Count ) { $writeCount }
    }
}

function Remove-InfluxData {
    <#
    .Synopsis
        Deletes data from Influx.

    .Description
        Removes a time range of data from an Influx bucket matching the predicate condition.

        Use `New-InfluxConnection` to create the Influx connection information.

        Use `Test-Influx` to check whether a given connection is healthy.

        Use `Get-InfluxData` to query data from Influx.

        Use `Add-InfluxData` to send data to Influx.

    .Notes
        This module requires the `InfluxDB.Client` NuGet package (`Install-Package InfluxDB.Client`).

        Disclaimer: This project is not affiliated with or endorsed by InfluxDB.

    .Link
        https://github.com/influxdata/influxdb-client-csharp
    .Link
        https://docs.influxdata.com/influxdb/v2.1/reference/syntax/delete-predicate/
    .Link
        Module inspired by https://github.com/jertel/vuegraf
    #>
    param(
        # Influx connection information.
        [Parameter(Mandatory, Position = 0, ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [InfluxConnection]$Connection
        ,
        # The start of the time range of data to delete.
        [Parameter(Position = 1, ValueFromPipelineByPropertyName)]
        [DateTime]$Start = (Get-Date -AsUTC '1900-01-01T00:00:00Z')
        ,
        # The end of the time range of data to delete.
        [Parameter(Position = 2, ValueFromPipelineByPropertyName)]
        [datetime]$End = (Get-Date -AsUTC)
        ,
        # An Influx [delete predicate](https://docs.influxdata.com/influxdb/v2.1/reference/syntax/delete-predicate/)
        # (e.g. 'key1="value1" and key2="value2"') to determine matching samples to delete from the time range.
        [Parameter(Position = 3, ValueFromPipelineByPropertyName)]
        [string]$Predicate = ''
        ,
        # A flag to automatically call `Start-InfluxDaemon` if `Test-Influx` returns false.
        [switch]$AutoStart
    )

    process {
        AutoStartInfluxDaemon $AutoStart $Connection

        $client = $Connection.GetDefaultClient()
        $deleteApi = $client.GetDeleteApi()

        Write-Verbose "Sending Influx request to delete data from $Start to $End matching '$Predicate'"
        $result = $deleteApi.Delete( $Start, $End, $Predicate, $Connection.Bucket, $Connection.Organization )
        $result.Wait()
    }
}


function Invoke-InfluxClient {
    <#
    .Synopsis
        Invokes the `influx.exe` client with a given command, automatically including
        standard host, bucket, organization, and token parameters if `Connection` is specified.

    .Description
        Searches the `Path` for the `influx.exe` client, and builds a full argument list by combining the
        `Command`, any standard parameters available in `Connection`, and then `CommandArguments`.

    .Notes
        Disclaimer: This project is not affiliated with or endorsed by InfluxDB.

    .Link
        https://docs.influxdata.com/influxdb/v2.1/reference/cli/
    #>
    [OutputType([string[]])]
    param(
        # The command passed to `influx.exe`.
        [Parameter(Mandatory, Position = 0)]
        [string[]]$Command
        ,
        # The command arguments passed to `influx.exe`.
        [Parameter(Position = 1, ValueFromRemainingArguments)]
        [string[]]$CommandArguments
        ,
        # Influx connection information.
        [Parameter(ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [InfluxConnection]$Connection
        ,
        # The direct path to the `influx.exe` client, or the path to the Influx installation to search for the `influx.exe` client.
        [Parameter(ValueFromPipelineByPropertyName)]
        [string]$Path = $(Join-Path $env:ProgramFiles 'InfluxData')
        ,
        # The name of the "host" or "url" flag for the current Influx command (or $null to skip `$Connection.Url`).
        [Parameter(ValueFromPipelineByPropertyName)]
        [string]$HostFlag = '--host'
        ,
        # The name of the "bucket" flag for the current Influx command (or $null to skip `$Connection.Bucket`).
        [Parameter(ValueFromPipelineByPropertyName)]
        [string]$BucketFlag = '--bucket'
        ,
        # The name of the "org" flag for the current Influx command (or $null to skip `$Connection.Organization`).
        [Parameter(ValueFromPipelineByPropertyName)]
        [string]$OrgFlag = '--org'
        ,
        # The name of the "token" flag for the current Influx command (or $null to skip `$Connection.AuthenticateToken`).
        [Parameter(ValueFromPipelineByPropertyName)]
        [string]$TokenFlag = '--token'
        ,
        # A flag to automatically call `Start-InfluxDaemon` if `Test-Influx` returns false.
        [switch]$AutoStart
    )

    process {
        AutoStartInfluxDaemon $AutoStart $Connection

        GetInfluxExe ([ref]$Path) 'influx.exe'

        $fullArguments = @($Command)
        if( $Connection.Url -and $HostFlag ) { $fullArguments += @($HostFlag, $Connection.Url) }
        if( $Connection.Bucket -and $BucketFlag ) { $fullArguments += @($BucketFlag, $Connection.Bucket) }
        if( $Connection.Organization -and $OrgFlag ) { $fullArguments += @($OrgFlag, $Connection.Organization) }
        if( $Connection.AuthenticateToken -and $TokenFlag ) {
            $fullArguments += @($TokenFlag, $Connection.AuthenticateToken)
            $tokenIndex = $fullArguments.Count - 1
        }
        $fullArguments += $CommandArguments

        $displayArguments = @($fullArguments)
        if( $tokenIndex ) {
            $token = $displayArguments[$tokenIndex]
            $displayArguments[$tokenIndex] = $token.Substring( 0, 3 ) + '...' + $token.Substring( $token.Length - 3 )
        }
        Write-Information "Invoking '$Path' $displayArguments"

        & $Path $fullArguments
    }
}



$script:WriteBatchSize = 1000
$script:WriteApiEventId = 'Add-InfluxData.WriteAPI.EventHandler'
$script:InfluxDaemonJobName = 'InfluxDB module Influx Daemon background job'

function GetInfluxExe( [ref][string]$path, [string]$target ) {
    if( [IO.Path]::GetExtension( $path.Value ) -ne '.exe' ) {
        Write-Verbose "Searching for $target client under '$($path.Value)'"
        $path.Value = Get-ChildItem -Path $path.Value -Recurse -Filter $target -ErrorAction SilentlyContinue |
            Select-Object -First 1 -ExpandProperty FullName
    }
}

function GetInfluxDaemon() {
    Get-Job -Name $script:InfluxDaemonJobName -ErrorAction SilentlyContinue
}

function AutoStartInfluxDaemon( [bool]$autoStart, [InfluxConnection]$connection ) {
    if( -not $autoStart -or -not $connection ) { return }

    if( Test-Influx $connection ) {
        Write-Verbose 'AutoStart: Test-Influx successfully connected to existing instance'
    }
    else {
        Write-Verbose 'AutoStart: calling Start-InfluxDaemon'
        Start-InfluxDaemon
    }
}

function AddTags( $builder, [hashtable]$tags, [string]$kind ) {
    if( $tags ) {
        $tags.GetEnumerator() | Sort-Object Key | ForEach-Object {
            Write-Verbose "Adding $kind tag $($_.Key) = $($_.Value)"
            if( $kind -eq 'default' ) {
                $builder = $builder.AddDefaultTag( $_.Key, $_.Value )
            }
            else {
                $builder = $builder.Tag( $_.Key, $_.Value )
            }
        }
    }

    $builder
}

function TryConvertToTimestamp( $timestamp ) {
    if( $null -ne $timestamp ) {
        [Timestamp]$timestamp
    }
    else {
        $null
    }
}

function IncrementTime( [nullable[NodaTime.Instant]]$timestamp, [InfluxDB.Client.Api.Domain.WritePrecision]$precision ) {
    if( $null -ne $timestamp ) {
        switch( $precision ) {
            'S' { $timestamp + [NodaTime.Duration]::FromSeconds( 1 ) }
            'Ms' { $timestamp + [NodaTime.Duration]::FromMilliseconds( 1 ) }
            'Us' { $timestamp + [NodaTime.Duration]::FromNanoseconds( 1000 ) }
            'Ns' { $timestamp + [NodaTime.Duration]::FromNanoseconds( 1 ) }
            default { throw "Unrecognized $($precision.GetType().FullName) value: $precision" }
        }
    }
}

function ProcessWriteEvents( ) {
    $writeEvents = Get-Event $script:WriteApiEventId -ErrorAction SilentlyContinue
    $writeEvents | Remove-Event
    foreach( $writeEvent in $writeEvents ) {
        $writeEventArgs = $writeEvent.SourceEventArgs
        if( $writeEventArgs -is [InfluxDB.Client.Writes.WriteSuccessEvent] ) {
            $lines = $writeEventArgs.LineProtocol.Split( "`n" ).Trim()
            $displayCount = $FormatEnumerationLimit -lt 0 ? $lines.Count : [Math]::Max( 0, $FormatEnumerationLimit - 2 )
            if( $lines.Count -gt ($displayCount + 2) ) { $lines = $lines[0..$displayCount] + @("...($($lines.Count - $displayCount - 2))...", $lines[-1]) }
            Write-Verbose "Write succeeded:`n$($lines -join "`n")"
        }
        else {
            throw ($writeEventArgs.Exception ?? $writeEventArgs)
        }
    }
}
