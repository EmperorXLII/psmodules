﻿#requires -Version 5


Write-Verbose 'Loading InfluxDB dependencies...'
Import-NuGet -PackageName System.Threading.Tasks.Extensions
Import-NuGet -PackageName Microsoft.Bcl.AsyncInterfaces
Import-NuGet -PackageName Microsoft.Extensions.ObjectPool
Import-NuGet -PackageName NodaTime
Import-NuGet -PackageName CsvHelper
Import-NuGet -PackageName RestSharp
Import-NuGet -PackageName Newtonsoft.Json
Import-NuGet -PackageName System.Configuration.ConfigurationManager
Import-NuGet -PackageName System.Reactive
Import-NuGet -PackageName InfluxDB.Client.Core
Import-NuGet -PackageName InfluxDB.Client
