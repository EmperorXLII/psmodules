﻿#requires -Version 5


class InfluxConnection {
    [string]$AuthenticateToken
    [string]$Organization
    [string]$Bucket
    [string]$Url = 'http://localhost:8086/'
    [TimeSpan]$Timeout = [TimeSpan]::FromMilliseconds( -1 )
    hidden [InfluxDB.Client.InfluxDBClient]$DefaultClient

    [InfluxDB.Client.InfluxDBClientOptions+Builder] CreateOptionsBuilder( ) {
        $builder = [InfluxDB.Client.InfluxDBClientOptions+Builder]::CreateNew().Url( $this.Url ).TimeOut( $this.Timeout ).ReadWriteTimeOut( $this.Timeout )

        if( $this.AuthenticateToken ) { $builder = $builder.AuthenticateToken( $this.AuthenticateToken ) }
        if( $this.Organization ) { $builder = $builder.Org( $this.Organization ) }
        if( $this.Bucket ) { $builder = $builder.Bucket( $this.Bucket ) }

        return $builder
    }

    [InfluxDB.Client.InfluxDBClient] CreateClient( ) { return $this.CreateClient( $null ) }
    [InfluxDB.Client.InfluxDBClient] CreateClient( [InfluxDB.Client.InfluxDBClientOptions]$options ) {
        $optionsValue = $options ?? $this.CreateOptionsBuilder().Build()
        Write-Verbose "Connecting to Influx address '$($optionsValue.Url)'."
        return [InfluxDB.Client.InfluxDBClientFactory]::Create( $optionsValue )
    }

    [InfluxDB.Client.InfluxDBClient] GetDefaultClient( ) {
        return ($this.DefaultClient ??= $this.CreateClient())
    }

    [string] ToString() {
        return '{0} in {1} at {2}' -f $this.Bucket, $this.Organization, $this.Url
    }
}

Update-TypeData -TypeName InfluxConnection -DefaultDisplayPropertySet 'Url', 'Organization', 'Bucket', 'Timeout'


class Timestamp : Tuple[DateTime, DateTimeOffset, NodaTime.Instant] {
    static [Timestamp]$MinValue = [DateTime]::new( 0, [DateTimeKind]::Utc )

    Timestamp( [NodaTime.Instant]$instant ) : base(
        $instant.ToDateTimeUtc(),
        $instant.ToDateTimeOffset(),
        $instant
    ) {
        $this.AddPublicMembers()
    }

    Timestamp( [DateTime]$dateTime ) : base(
        $dateTime.ToUniversalTime(),
        $dateTime,
        [NodaTime.Instant]::FromDateTimeOffset( $dateTime )
    ) {
        $this.AddPublicMembers()
    }

    Timestamp( [DateTimeOffset]$dateTimeOffset ) : base(
        $dateTimeOffset.UtcDateTime,
        $dateTimeOffset,
        [NodaTime.Instant]::FromDateTimeOffset( $dateTimeOffset )
    ) {
        $this.AddPublicMembers()
    }

    [int] CompareTo( [object]$obj ) { return $this.Instant.CompareTo( $obj.Instant ) }

    [Timestamp] AddSeconds( [double]$value ) { return $this.DateTime.AddSeconds( $value ) }
    [Timestamp] AddHours(   [double]$value ) { return $this.DateTime.AddHours(   $value ) }
    [Timestamp] AddDays(    [double]$value ) { return $this.DateTime.AddDays(    $value ) }

    [string] ToString() {
        return $this.DateTimeOffset.ToLocalTime().ToString()
    }

    [string] ToQueryString() {
        # https://docs.influxdata.com/influxdb/cloud/reference/glossary/#rfc3339-timestamp
        # https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-date-and-time-format-strings#Roundtrip
        return $this.DateTime.ToString( 'o' )
    }

    # Adds:
    # - readonly         [DateTime] $DateTime
    # - readonly   [DateTimeOffset] $DateTimeOffset
    # - readonly [NodaTime.Instant] $Instant
    hidden AddPublicMembers() {
        # https://stackoverflow.com/questions/41663213/how-to-create-a-read-only-property-in-a-powershell-5-0-class
        $properties = [TimestampConverter]::SupportedTypes
        for( $i = 1; $i -le $properties.Count; $i += 1 ) {
            $name = $properties[$i - 1].Name
            $getter = [scriptblock]::Create( "return `$this.Item$i" )
            Add-Member -InputObject $this -Name $name -MemberType ScriptProperty -Value $getter -SecondValue { Write-Warning "$name is a read-only property" }
        }
    }
}

# https://stackoverflow.com/questions/59678198/typeconverter-registration-throws-nullrefexception-in-powershell
class TimestampConverter : System.Management.Automation.PSTypeConverter {
    static hidden [Type[]]$SupportedTypes = @([DateTime], [DateTimeOffset], [NodaTime.Instant])

    [bool] CanConvertFrom( [object]$object, [Type]$destinationType ) {
        return [TimestampConverter]::SupportedTypes -contains $object.GetType()
    }

    [bool] CanConvertTo( [object]$object, [Type]$destinationType ) {
        return [TimestampConverter]::SupportedTypes -contains $destinationType
    }

    [object] ConvertFrom( [object]$sourceValue, [Type]$destinationType, [IFormatProvider]$formatProvider, [bool]$ignoreCase ) {
        return [Timestamp]::new( $sourceValue )
    }

    [object] ConvertTo( [object]$sourceValue, [Type]$destinationType, [IFormatProvider]$formatProvider, [bool]$ignoreCase ) {
        $member = $destinationType.Name
        return $sourceValue.$member
    }
}

Update-TypeData -TypeName Timestamp -DefaultDisplayPropertySet @([TimestampConverter]::SupportedTypes.Name) -TypeConverter TimestampConverter


class SampleData {
    # (optional) Default tags for all samples.
    [hashtable]$Tags = @{}
    # (optional) Timestamp of first sample.
    [Timestamp]$Timestamp = $null
    [array]$Samples
}
